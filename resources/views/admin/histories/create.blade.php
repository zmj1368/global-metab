@extends('admin.calendars.layout')
<section class="box box-info">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-12">
                <div class="flex-center position-ref full-height">
                    <div class="NoticeTab">
                        内容设置
                    </div>
                    <form action="" method="post" accept-charset="UTF-8" class="form-horizontal"
                          enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="fields-group">
                                @include('layout.partOne')
                                <div class="form-group">
                                    <label for="name" class="col-sm-2  control-label">分类</label>
                                    <div class="col-sm-8 marTop05">
                                        <div id="content-type">
                                            @if('create' == $act)
                                                <label><input type="radio" name="cat" id="" value="1" checked="checked">
                                                    &nbsp;代谢简史</label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <label><input type="radio" name="cat" id="" value="2">
                                                    &nbsp;洞见未来</label>
                                            @else
                                                <label><input type="radio" name="cat" id="" value="1"
                                                              @if(isset($data['cat']) && 1 == intval($data['cat'])) checked="checked" @endif>
                                                    &nbsp;代谢简史</label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <label><input type="radio" name="cat" id="" value="2"
                                                              @if(isset($data['cat']) && 2 == intval($data['cat'])) checked="checked" @endif>
                                                    &nbsp;洞见未来</label>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="extraDiv" data-id="extra">
                                    <label for="cell" class="col-sm-2 control-label">关键字</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            @if('create' == $act)
                                                <input type="text" id="extra" name="extra" style="width: 500px" value=""
                                                       maxlength="6"
                                                       class="form-control extra"
                                                       placeholder="您可以填年份，也可以填其它关键字，关键字建议不超过4个字">
                                            @else
                                                <input type="text" id="extra" name="extra" style="width: 500px"
                                                       @if(isset($data['extra']))
                                                       value="{{ $data['extra'] }}"
                                                       @else
                                                       value=""
                                                       @endif
                                                       maxlength="6" class="form-control extra"
                                                       placeholder="您可以填年份，也可以填其它关键字，关键字建议不超过4个字">
                                            @endif
                                            <span class="star">★</span>
                                        </div>
                                    </div>
                                </div>
                                @include('layout.partTwo')
                            </div>

                        </div>
                        <!-- /.box-body -->

                        @include('layout.footer')
                    </form>
                </div>
            </div>
        </div>
    </div>

    @if($act == 'create')
        <input type="hidden" name="tagsStr" id="" value="">
    @else
        <input type="hidden" name="tagsStr" id=""
               @if(isset($data['tags']))
               value="{{ $data['tags'] }}"
               @else
               value=""
                @endif
        >
    @endif
    <input type="hidden" name="content_type_editor" id=""
           @if(isset($data['content_type']))
           value="{{ $data['content_type'] }}"
           @else
           value="1"
            @endif
    >
</section>

<script type="text/javascript" src="{{ URL::asset('/js/layout/layout.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/histories/index.js') }}"></script>