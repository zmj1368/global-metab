@extends('admin.calendars.layout')
<section class="box box-info">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-12">
                <div class="flex-center position-ref full-height">
                    <div class="NoticeTab">
                        内容设置
                    </div>
                    <form action="" method="post" accept-charset="UTF-8" class="form-horizontal"
                          enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="fields-group">
                                @include('layout.partOne')
                                <div class="form-group">
                                    <label for="name" class="col-sm-2  control-label">分类</label>
                                    <div class="col-sm-8">
                                        <div class="input-group marTop05">
                                            <div class="input-group">
                                                @if('create' == $act)
                                                @foreach($meetingsArr as $val)
                                                <label><input type="radio" name="cate" value="{{ $val['id'] }}">
                                                    &nbsp;{{ $val['name'] }}</label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                @endforeach
                                                @else
                                                @foreach($meetingsArr as $val)
                                                <label><input type="radio" name="cate" value="{{ $val['id'] }}"
                                                              @if(isset($data['cate_id']) && intval($val['id']) ==
                                                    intval($data['cate_id']))
                                                    checked="checked"
                                                    @endif>
                                                    &nbsp;{{ $val['name'] }}</label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @include('layout.partTwo')
                        </div>

                </div>
                <!-- /.box-body -->

                @include('layout.footer')
                </form>
            </div>
        </div>
    </div>
    </div>

    @if($act == 'create')
    <input type="hidden" name="tagsArr" id="" value="">
    @else
    <input type="hidden" name="tagsArr" id=""
           @if(isset($data['tags']))
    value="{{ $data['tags'] }}"
    @else
    value=""
    @endif
    >
    @endif
    <input type="hidden" name="content_type_editor" id=""
           @if(isset($data['content_type']))
    value="{{ $data['content_type'] }}"
    @else
    value="1"
    @endif
    >
</section>

<script type="text/javascript" src="{{ URL::asset('/js/layout/layout.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/meeting/index.js') }}"></script>