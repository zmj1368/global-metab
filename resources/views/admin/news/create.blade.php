@extends('admin.calendars.layout')
<section class="box box-info">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-12">
                <div class="flex-center position-ref full-height">
                    <div class="NoticeTab">
                        内容设置
                    </div>
                    <form action="" method="post" accept-charset="UTF-8" class="form-horizontal"
                          enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="fields-group">
                                @include('layout.partOne')
                                <div class="form-group">
                                    <label for="name" class="col-sm-2  control-label">分类</label>
                                    <div class="col-sm-8">
                                        <div class="input-group marTop05">
                                            <div class="input-group">
                                                @if('create' == $act)
                                                    @foreach($newsArr as $val)
                                                        <label><input type="radio" name="cate" value="{{ $val['id'] }}">
                                                            &nbsp;{{ $val['name'] }}</label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                    @endforeach
                                                @else
                                                    @foreach($newsArr as $val)
                                                        <label><input type="radio" name="cate" value="{{ $val['id'] }}"
                                                                      @if(isset($data['cate_id']) && intval($val['id']) ==
                                                            intval($data['cate_id']))
                                                                      checked="checked"
                                                                    @endif>
                                                            &nbsp;{{ $val['name'] }}</label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="authorTextDiv">
                                    <label for="cell" class="col-sm-2 control-label">作者</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            @if('create' == $act)
                                                <input type="text" id="author" name="author" style="width: 500px"
                                                       value="" class="form-control author"
                                                       placeholder="不填默认代谢网 多位作者请用空格隔开">
                                            @else
                                                <input type="text" id="author" name="author" style="width: 500px"
                                                       @if(isset($data['author']))
                                                       value="{{ $data['author'] }}"
                                                       @else
                                                       value=""
                                                       @endif
                                                       class="form-control author" placeholder="不填默认代谢网 多位作者请用空格隔开">
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="authorArrDiv" style="display: none;">
                                    <label for="cell" class="col-sm-2 control-label">作者</label>
                                    <div class="col-sm-8">
                                        <div class="input-group marTop05">
                                            <select name="authors[]" class="form-control" multiple="multiple"
                                                    id="authors_list">
                                                @foreach($authorArr as $val)
                                                    <option value="{{$val['id']}}">{{$val['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-2  control-label">推荐到首页</label>
                                    <div class="col-sm-8">
                                        <div class="input-group marTop05">
                                            <div class="input-group">
                                                @if('create' == $act)
                                                    <label><input type="radio" name="is_recommend" value="1"
                                                                  checked="checked">&nbsp;&nbsp;推荐</label>&nbsp;&nbsp;
                                                    &nbsp;&nbsp;
                                                    <label><input type="radio" name="is_recommend" value="0">&nbsp;&nbsp;不推荐</label>
                                                @else
                                                    <label><input type="radio" name="is_recommend"
                                                                  value="1"
                                                                  @if(isset($data['is_recommend']) && intval($data['is_recommend']) == 1)
                                                                  checked="checked"
                                                                @endif>&nbsp;&nbsp;推荐</label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    <label><input type="radio" name="is_recommend" value="0"
                                                                  @if(isset($data['is_recommend']) && intval($data['is_recommend']) == 0)
                                                                  checked="checked"
                                                                @endif>&nbsp;&nbsp;不推荐</label>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @include('layout.partTwo')
                            </div>

                        </div>
                        <!-- /.box-body -->

                        @include('layout.footer')
                    </form>
                </div>
            </div>
        </div>
    </div>
    @if($act == 'create')
        <input type="hidden" name="tagsStr" id="" value="">
    @else
        <input type="hidden" name="tagsStr" id=""
               @if(isset($data['tags']))
               value="{{ $data['tags'] }}"
               @else
               value=""
                @endif
        >
    @endif
    @if($act == 'create')
        <input type="hidden" name="authorsStr" id="" value="">
    @else
        <input type="hidden" name="authorsStr" id=""
               @if(isset($data['author']))
               value="{{ $data['author'] }}"
               @else
               value=""
                @endif
        >
    @endif
    <input type="hidden" name="content_type_editor" id=""
           @if(isset($data['content_type']))
           value="{{ $data['content_type'] }}"
           @else
           value="1"
            @endif
    >
    <input type="hidden" name="noticeStore" id="" value="{{ route($host . '.store') }}">
    <input type="hidden" name="noticeSucRedirect" id="" value="{{ route($host . '.index') }}">
</section>

<script type="text/javascript" src="{{ URL::asset('/js/layout/layout.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/news/index.js') }}"></script>