<section class="box box-info">
    {{ csrf_field() }}
    <div class="box-header with-border">
        <h3 class="box-title">基本信息</h3>
    </div>
    <style>
        .star {
            color: #FF3300;
            margin-left: 10px;
            line-height: 34px;
            position: absolute;
        }
    </style>
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-12">
                <div class="flex-center position-ref full-height">
                    <form action="" method="post" accept-charset="UTF-8" class="form-horizontal"
                          enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="fields-group">
                                <div class="form-group">
                                    <label for="cell" class="col-sm-2  control-label">标题</label>
                                    <div class="col-sm-8">
                                        <div class="input-group inline">
                                            <input type="text" id="title" name="title" style="width: 500px"
                                                   @if('create' == $act)
                                                   value=""
                                                   @else
                                                   value="{{ $data['title'] }}"
                                                   @endif
                                                   class="form-control title" placeholder="标题">
                                        </div>
                                        <span class="star">★</span>
                                    </div>
                                </div>
                                <div class="form-group" data-id="cover">
                                    <label for="" class="control-label col-md-2">图片</label>
                                    <div class="col-md-5">
                                        @if('show' == $act)
                                            <div>
                                                <input type="hidden" name="origin_doc_photo_url" value="">
                                                <input type="hidden" id="new_doc_photo"
                                                       @if(!$data['image']) value="{{ $data['image'] }}" @else value=""
                                                       @endif name="cover_img">
                                                <img id="doc_photo"
                                                     src="@if(!$data['image']) {{ App\Models\Ads::DEFAULT_PHOTO }} @else {{ $data['image'] }}@endif"
                                                     class="image_size" alt="链接图片" width="160" height="77"
                                                     data-width="800"
                                                     data-height="385">
                                            </div>
                                        @else
                                            <div>
                                                <input type="hidden" name="origin_doc_photo_url" value="">
                                                <input type="hidden" id="new_doc_photo"
                                                       @if('edit' == $act && $data['image']) value="{{ $data['image'] }}"
                                                       @else value="" @endif name="cover_img">
                                                <img id="doc_photo"
                                                     src="@if('edit' == $act && $data['image']){{ $data['image'] }}@else{{ App\Models\Ads::DEFAULT_PHOTO }}@endif"
                                                     class="image_size" alt="链接图片" width="240" height="100"
                                                     data-width="720"
                                                     data-height="300">
                                            </div>
                                            <div style="margin-top: 10px">
                                                <input type="file" class="hidden" name="doc_photo_file"
                                                       id="doc_photo_file" value="浏览..."
                                                       accept="image/gif,image/png,image/jpeg,image/jpg,image/bmp">
                                                <a class="btn btn-info" id="upload">上传</a><span class="margin">建议上传尺寸：720X300</span>
                                            </div>
                                        @endif
                                    </div>
                                    <span class="star">★</span>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2  control-label">位置</label>
                                    <div class="col-sm-8">
                                        <div class="input-group" style="display: inline-block;">
                                            @if('create' == $act)
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="space[]" value="1" class="space">首页
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="space[]" value="2" class="space">会议追踪
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="space[]" value="3" class="space">专家解读
                                                </label>
                                            @else
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="space[]" value="1"
                                                           @if (in_array(1, $data['spaceArr']))
                                                           checked="checked"
                                                           @endif
                                                           class="space">首页
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="space[]" value="2"
                                                           @if (in_array(2, $data['spaceArr']))
                                                           checked="checked"
                                                           @endif
                                                           class="space">会议追踪
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="space[]" value="3"
                                                           @if (in_array(3, $data['spaceArr']))
                                                           checked="checked"
                                                           @endif
                                                           class="space">专家解读
                                                </label>
                                            @endif
                                        </div>
                                        <span class="star">★</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2  control-label">排序</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="number" id="level" style="width: 100px" name="level"
                                                   @if('create' == $act)
                                                   value="0"
                                                   @else
                                                   value="{{ $data['sort'] }}"
                                                   @endif
                                                   min="0" step="1"
                                                   class="form-control title" placeholder="0">&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span style="color: red;line-height: 34px">序号越大，排序越靠前；相同序号的，创建时间越近越靠前</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2  control-label">状态</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <select name="status" style="width: 80px;" id="status" class="form-control">
                                                @if('create' == $act)
                                                    <option value="1" selected="selected">显示</option>
                                                    <option value="0">隐藏</option>
                                                @else
                                                    <option value="1"
                                                            @if(1 == intval($data['status'])) selected="selected" @endif>
                                                        显示
                                                    </option>
                                                    <option value="0"
                                                            @if(0 == intval($data['status'])) selected="selected" @endif>
                                                        隐藏
                                                    </option>
                                                @endif
                                            </select>
                                            <span class="star">★</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2  control-label">链接地址</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="text" id="link" style="width: 500px" name="link"
                                                   @if('create' == $act)
                                                   value=""
                                                   @else
                                                   value="{{ $data['link'] }}"
                                                   @endif
                                                   class="form-control title" placeholder="链接(例如:http://www.baidu.com)">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->

                        @if('create' == $act)
                            <div class="box-footer">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-1">
                                    <div class="btn-group pull-left">
                                        <button type="button" class="btn btn-info pull-right" id="btn_create_doc">创&nbsp;建
                                        </button>

                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-2">
                                    <div class="btn-group pull-left">
                                        <button type="reset" id="backBtn" class="btn btn-warning">返&nbsp;回</button>
                                    </div>
                                </div>
                            </div>
                        @elseif('show' == $act)
                            <div class="box-footer">
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-1">
                                    <div class="btn-group pull-left">
                                        <button type="reset" id="backBtn" class="btn btn-warning">返&nbsp;回</button>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="box-footer">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-1">
                                    <div class="btn-group pull-left">
                                        {{--<button type="submit" class="btn btn-primary">提交</button>--}}
                                        <button type="button" class="btn btn-info pull-right" id="btn_edit_doc">更&nbsp;新</button>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-2">
                                    <div class="btn-group pull-left">
                                        <button type="reset" id="backBtn" class="btn btn-warning">返&nbsp;回</button>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="uploadPhotoUrl" id="" value='{{ route('upload.image') }}'>

    <input type="hidden" name="adsStore" id="" value="{{ route('ads.store') }}">
    <input type="hidden" name="adsRedirect" id="" value="{{ route('ads.index') }}">
    @if('edit' == $act)
        <input type="hidden" name="adsUpdate" id="" value="{{ route('ads.update', $data['id']) }}">
    @endif

</section>

<div id="showImg" class="modal fade my-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-body" style="margin-top: 20%">
            <img width="800" height="385">
        </div>
    </div>
</div>

<script src="{{ URL::asset('/js/ads/index.js') }}"></script>
@if('show' == $act)
    <script>
        // 禁止操作
        $(function () {
            $('input').attr('disabled', 'true');
            $('select').attr('disabled', 'true');
        })
    </script>
@endif