<link rel="stylesheet" href="{{ URL::asset('/vendor/bootstrap-table/dist/bootstrap-table.min.css') }}">
<style>
    .inline {
        display: inline;
        margin-right: 10px;
    }
</style>
{{ csrf_field() }}
<section class="invoice">
    <div class="row">
        <div class="col-xs-12">
            <div class="form-horizontal form-inline form-group">
                <div class="inline">
                    <label class="control-label" style="vertical-align: top;">标题: </label>&nbsp;&nbsp;
                    <input type="text" value="" placeholder="请输入标题" class="form-control searchTitle"
                           id="searchTitle">
                </div>
                <div class="inline">
                    <label class="control-label" style="vertical-align: top;">位置: </label>&nbsp;&nbsp;
                    <select class="form-control filter_input_sm" id="searchSpace" title="">
                        <option value="0">请选择</option>
                        <option value="-1">全部位置</option>
                        <option value="1">首页</option>
                        <option value="2">会议追踪</option>
                        <option value="3">专家解读</option>
                    </select>
                </div>
                <div class="inline">
                    <label class="control-label" style="vertical-align: top;">是否显示: </label>&nbsp;&nbsp;
                    <select class="form-control filter_input_sm" id="searchStatus" title="">
                        <option value="-1">请选择</option>
                        <option value="1">显示</option>
                        <option value="0">隐藏</option>
                    </select>
                </div>
                <div class="inline">
                    <a class="btn btn-info" role="button" data-type="1" id="search">查询</a>
                    <a class="btn btn-default" role="button" data-type="0" id="reset">清空查询</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-bottom: 10px">
        <div class="col-sm-12">
            <a class="btn btn-info pull-right" role="button" href="{{ route('ads.create') }}"><span class="fa fa-plus-circle"></span>&nbsp;&nbsp;添加广告</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <table id="table" class="table table-striped" style="word-break:break-all; word-wrap:break-all;"></table>
        </div>
    </div>

    <input type="hidden" id="listUrl" value="{{ route('ads.list') }}">
    <input type="hidden" id="delUrl" value="{{ route('ads.del') }}">
</section>
{{--删除广告--}}
<div id="modal_offlineDoctor" class="modal fade" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">删除广告</h4>
            </div>
            <div class="modal-body">
                <p>您确认删除该广告？</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary offlineDoc" data-id="" data-status="">保存</button>
            </div>
        </div>
    </div>
</div>
{{--删除广告结束--}}
<script src="{{ URL::asset('/vendor/bootstrap-table/dist/bootstrap-table.min.js') }}"></script>
<script src="{{ URL::asset('/js/ads/list.js') }}"></script>
