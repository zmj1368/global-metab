<link rel="stylesheet" href="{{ URL::asset('/vendor/bootstrap-table/dist/bootstrap-table.min.css') }}">
<style>
    span.select2 {
        font-size: 14px;
    }

    a.Detail {
        cursor: pointer;
    }

    .project-name {
        max-width: 100px;
        height: 50px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    .project-name:hover::after {
        content: attr(data-name);
        display: block;
        position: absolute;
        margin-left: 70px;
        margin-top: 5px;
        border: 1px solid #dcdcdc;
        font-size: 14px;
        border-radius: 4px;
        z-index: 999;
        background: lightgoldenrodyellow;
    }

    #update_qrcode {
        margin-left: 160px;
        margin-top: 110px
    }

    #qrcode_show {
        padding-left: 80px;
        min-height: 300px;
    }
</style>
<section class="invoice">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- title start -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <div>
                    <div class="form-inline">
                        <input type="hidden" id="default_hosp_id" value="">
                        <input type="hidden" id="default_dept_id" value="">
                        <input type="hidden" id="hosp_id" value="">
                        <div class="form-group">
                            <input type="text" name="title_selector" id="title_selector"
                                   class="form-control filter_input_sm"
                                   placeholder="标题"
                                   value="">
                            <a class="btn btn-info" role="button" data-type="1" id="search">查询</a>
                            <a class="btn btn-default" role="button" data-type="0" id="reset">清空查询</a>
                        </div>

                        <div class="pull-right">
                            <a onclick="hrefAdd()" class="btn btn-info">
                                <span class="fa fa-plus-circle"></span>
                                新增日历
                            </a>
                        </div>
                    </div>
                </div>

                <div class=" pull-right">
                    <small></small>
                </div>

            </h2>
        </div>
    </div>
    <!-- title end -->

    <!-- list start -->
    <div class="row">
        <div class="col-sm-12">
            <table id="table" class="table table-striped"></table>
        </div>
    </div>
    <input type="hidden" id="listUrl" value="{{ route('calendars.list') }}">
    <input type="hidden" name="" id="key_words_doc_dept" value="0">
    <input type="hidden" name="" id="delUrl" value="{{ route('calendars.del') }}">
    <!-- list end -->
</section>

{{--删除日历--}}
<div id="modal_offlineDoctor" class="modal fade" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">删除日历</h4>
            </div>
            <div class="modal-body">
                <p>您确认删除该日历？</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">不删了</button>
                <button type="button" class="btn btn-primary offlineDoc" data-id="" data-status="">删除</button>
            </div>
        </div>
    </div>
</div>
{{--删除日历结束--}}
<script src="{{ URL::asset('/vendor/bootstrap-table/dist/bootstrap-table.min.js') }}"></script>
<script src="{{ URL::asset('/js/calendars/list.js') }}"></script>