<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="{{ URL::asset('vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet"
      media="screen">
<style>
    .Tab {
        width: 100%;
        text-align: center;
        font-size: 18px;
    }

    .marTop05 {
        margin-top: 0.5rem;
    }

    .marTop1 {
        margin-top: 1rem;
    }

    .marTop2 {
        margin-top: 2rem;
    }

    .TabTitle {
        text-align: center;
    }

    .TabActive {
        font-size: 20px;
        font-weight: bold;
    }

    .NoticeTab {
        font-size: 18px;
        font-weight: bold;
        margin-top: 18px;
    }

    .yulanbtn {
        width: 100px;
    }

    .selectTab {
        font-size: 16px;
        margin-top: 18px;
    }

    .selectDiv {
        width: 100px;
        display: inline-block
    }

    .width5 {
        width: 5%;
    }

    .width10 {
        width: 10%;
    }

    .width15 {
        width: 15%;
    }

    .width20 {
        width: 20%;
    }

    .textCenter {
        text-align: center;
    }

    .addAuthorBtnBiv {
        font-size: 16px;
    }

    .showAuthorDiv {
        border: 1px solid #000000;
        width: 60rem;
        padding: 2rem;
    }

    .showAuthorDivTag {
        font-weight: bold;
        font-size: 16px;
        margin-bottom: 5px;
    }

    .showAuthorDiv input {
        width: 30%;
        display: inline-block;
    }

    .showAuthorDiv .checkAuthorBtn {
        border-radius: 10px;
        width: 15%;
        margin-left: 20%;
    }

    .showAuthorTable {
        width: 100%;
        border: 1px solid #000000;
        text-align: center;
        margin-top: 1rem;
        line-height: 2rem;
        font-size: 1.5rem;
    }

    .showAuthorTable td {
        border: 1px solid #000;
    }

    .copyBtn {
        cursor: pointer;
    }

    .my-modal {
        text-align: center;
        padding-top: 150px;
    }
</style>
<script>
    $(function () {
        @if(session('successTag'))
        toastr.success("{{ session('successTag') }}");
        {{ session()->forget('successTag') }}
        @endif
        @if(session('errorTag'))
        toastr.error("{{ session('errorTag') }}");
        {{ session()->forget('errorTag') }}
        @endif
        // 清空
        $('#clearBtn').on('click', function () {
            window.location.href = "{{ route($host . '.index') }}"
        })
    })
</script>
<link rel="stylesheet" href="{{ URL::asset('/vendor/bootstrap-table/dist/bootstrap-table.min.css') }}">
<script src="{{ URL::asset('/vendor/bootstrap-table/dist/bootstrap-table.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('vendor/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.fr.js') }}"></script>
<script type="text/javascript">
    $('.form_datetime').datetimepicker({

        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        todayHighlight: true,   //当天高亮显示
        minView: "month",   //不显示时分秒
        clearBtn:true,
        dayStep: 1,
        startDate:new Date(), // 今天以前日期不能选择
    });
</script>