@extends('admin.calendars.layout')
<section class="box box-info">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-12">
                <div class="flex-center position-ref full-height">
                    <div class="NoticeTab">
                        内容设置
                    </div>
                    <form action="" method="post" accept-charset="UTF-8" class="form-horizontal"
                          enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="fields-group">
                                <div class="form-group">
                                    <label for="cell" class="col-sm-2 control-label">标题</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="text" id="title" name="title" style="width: 500px" value="{{ $data['title'] }}"
                                                   class="form-control title" placeholder="标题(最多14个字)" maxlength="14">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-2  control-label">标签</label>
                                    <div class="col-sm-8">
                                        <div class="input-group marTop05">
                                            <select name="join_tags[]" class="form-control" multiple="multiple" id="tags_list">
                                                @foreach($tagsArr as $val)
                                                    <option value="{{$val['id']}}">{{$val['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" data-id="cover">
                                    <label for="" class="control-label col-md-2">背景图片</label>
                                    <div class="col-md-5 marTop05">
                                        <div>
                                            <input type="hidden" name="origin_image_url" value="{{ $data['image'] ?? '' }}">
                                            <input type="hidden" id="new_image" name="image" value="{{ $data['image'] ?? '' }}">
                                            @if(!empty($data['image']))
                                                <img id="doc_photo" src="{{ $data['image'] ?? '' }}" class="image_size" alt="背景图片" width="286" height="325" data-width="286"
                                                     data-height="325">
                                            @else
                                                <img id="doc_photo" src="{{ App\Models\Calendars::DEFAULT_PHOTO }}" class="image_size" alt="背景图片" width="286" height="325" data-width="286"
                                                     data-height="325">
                                            @endif
                                        </div>
                                        <div class="marTop1">
                                            <input type="file" class="hidden" name="image_file"
                                                   id="image_file" value="浏览..."
                                                   accept="image/gif,image/png,image/jpeg,image/jpg,image/bmp">
                                            <a class="btn btn-info" id="upload">上传</a><span class="margin">建议上传尺寸：286X325</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-2  control-label">正文</label>
                                    <div class="col-sm-8">
                                        <div id="contentEditor" class="marTop1">
                                            <div id="editor">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-1">
                                <div class="btn-group pull-left">
                                    {{--<button type="submit" class="btn btn-primary">提交</button>--}}
                                    <button type="button" class="btn btn-primary pull-right" id="btn_Editor">更&nbsp;新
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <div class="btn-group pull-left">
                                    <button type="reset" class="btn btn-warning">撤&nbsp;销</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <textarea class="hidden" id="content" name="content">{{ empty($data['content']) ? '' : $data['content'] }}</textarea>
    <input type="hidden" name="tagsStr" id="" value='{{ $data['tags'] }}'>
    <input type="hidden" name="uploadPhotoUrl" id="" value='{{ route('upload.image') }}'>

    <input type="hidden" name="noticeStore" id="" value="{{ route($host . '.update', $data['id']) }}">
    <input type="hidden" name="noticeSucRedirect" id="" value="{{ route($host . '.index') }}">
</section>
<div id="showImg" class="modal fade my-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-body">
            <img width="380" height="260">
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ URL::asset('vendor/wangEditor-3.1.1/release/wangEditor.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/calendars/edit.js') }}"></script>