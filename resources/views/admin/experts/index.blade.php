<link rel="stylesheet" href="{{ URL::asset('/vendor/bootstrap-table/dist/bootstrap-table.min.css') }}">
<style>
    span.select2 {
        font-size: 14px;
    }

    a.Detail {
        cursor: pointer;
    }

    .project-name {
        max-width: 100px;
        height: 50px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    .project-name:hover::after {
        content: attr(data-name);
        display: block;
        position: absolute;
        margin-left: 70px;
        margin-top: 5px;
        border: 1px solid #dcdcdc;
        font-size: 14px;
        border-radius: 4px;
        z-index: 999;
        background: lightgoldenrodyellow;
    }

    #update_qrcode {
        margin-left: 160px;
        margin-top: 110px
    }

    #qrcode_show {
        padding-left: 80px;
        min-height: 300px;
    }
</style>
<section class="invoice">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- title start -->
    <div class="row">
        <div class="col-xs-12">
            <div class="form-horizontal form-inline form-group">
                <div class="inline">
                    <label class="control-label" style="vertical-align: top;">专家名字: </label>&nbsp;&nbsp;
                    <input type="text" value="" placeholder="请输入专家名字" class="form-control searchTitle"
                           id="searchTitle" name="searchTitle">
                </div>
                <div class="inline">
                    <label class="control-label" style="vertical-align: top;">是否启用: </label>&nbsp;&nbsp;
                    <select class="form-control filter_input_sm" id="searchStatus" name="searchStatus" title="">
                        <option value="-1">请选择</option>
                        <option value="1">启用</option>
                        <option value="0">不启用</option>
                    </select>
                </div>
                <div class="inline">
                    <a class="btn btn-info" role="button" data-type="1" id="search">查询</a>
                    <a class="btn btn-default" role="button" data-type="0" id="reset">清空查询</a>
                </div>
            </div>
        </div>
    </div>


    <div class="row" style="margin-bottom: 10px">
        <div class="col-sm-12">
            <a class="btn btn-info pull-right" role="button" href="{{ route('experts.create') }}"><span class="fa fa-plus-circle"></span>&nbsp;添加专家</a>
        </div>
    </div>
    <!-- title end -->

    <!-- list start -->
    <div class="row">
        <div class="col-sm-12">
            <table id="table" class="table table-striped"></table>
        </div>
    </div>
    <input type="hidden" id="listUrl" value="{{ route('experts.list') }}">
    <input type="hidden" name="" id="key_words_doc_dept" value="0">
    <input type="hidden" name="" id="delUrl" value="{{ route('experts.del') }}">
    <!-- list end -->

</section>

{{--删除专家--}}
<div id="modal_offlineDoctor" class="modal fade" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">删除专家</h4>
            </div>
            <div class="modal-body">
                <p>您确认删除该专家？</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary offlineDoc" data-id="" data-status="">保存</button>
            </div>
        </div>
    </div>
</div>
{{--删除专家结束--}}

<script src="{{ URL::asset('/vendor/bootstrap-table/dist/bootstrap-table.min.js') }}"></script>
<script src="{{ URL::asset('/js/experts/list.js') }}"></script>