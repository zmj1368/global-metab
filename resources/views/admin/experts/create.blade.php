@extends('admin.experts.layout')
<section class="box box-info">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-12">
                <div class="flex-center position-ref full-height">
                    <div class="NoticeTab">
                        创建专家
                    </div>
                    <form action="" method="post" accept-charset="UTF-8" class="form-horizontal"
                          enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="fields-group">
                                <div class="form-group">
                                    <label for="cell" class="col-sm-2 control-label">专家</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="text" id="name" name="name" style="width: 500px" value=""
                                                   class="form-control title" placeholder="请输入专家姓名(最多10个字)" maxlength="10">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" data-id="cover">
                                    <label for="" class="control-label col-md-2">图片</label>
                                    <div class="col-md-8 marTop05">
                                        <div>
                                            <input type="hidden" name="origin_image_url" value="">
                                            <input type="hidden" id="new_image" name="image">
                                            <img id="doc_photo"
                                                 src="{{App\Models\Ads::DEFAULT_PHOTO}}"
                                                 class="image_size" alt="背景图片" width="150" height="100" data-width="300"
                                                 data-height="200">
                                        </div>
                                        <div class="marTop1">
                                            <input type="file" class="hidden" name="image_file"
                                                   id="image_file" value="浏览..."
                                                   accept="image/gif,image/png,image/jpeg,image/jpg,image/bmp">
                                            <a class="btn btn-info" id="upload">上传</a><span class="margin">上传尺寸比例为：高：宽=2:3；图片大小限制在2M内；图片格式支持：jpg、png、jpeg</span>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="name" class="col-sm-2  control-label">状态</label>
                                    <div class="col-sm-8">
                                        <div class="input-group marTop05">
                                            <label>
                                                <input type="radio" name="status" id="" value="1" checked="checked"> 启用&nbsp;
                                            </label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="radio" name="status" id="" value="0">
                                                &nbsp;不启用
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-2  control-label">介绍</label>
                                    <div class="col-sm-8">
                                        <div id="contentEditor" class="marTop1">
                                            <div id="editor">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-1">
                                <div class="btn-group pull-left">
                                    {{--<button type="submit" class="btn btn-primary">提交</button>--}}
                                    <button type="button" class="btn btn-info pull-right" id="btn_submit">创&nbsp;建
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <div class="btn-group pull-left">
                                    <button type="reset" class="btn btn-warning">撤&nbsp;销</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="noticeEditor" id="" value="{{ route('editor.upload.image') }}">
    <input type="hidden" name="uploadPhotoUrl" id="" value='{{ route('upload.image') }}'>

    <input type="hidden" name="noticeStore" id="" value="{{ route($host . '.store') }}">
    <input type="hidden" name="noticeSucRedirect" id="" value="{{ route($host . '.index') }}">
</section>
<div id="showImg" class="modal fade my-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-body">
            <img width="380" height="260">
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ URL::asset('vendor/wangEditor-3.1.1/release/wangEditor.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/experts/index.js') }}"></script>