@extends('admin.calendars.layout')
<section class="box box-info">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-12">
                <div class="flex-center position-ref full-height">
                    <div class="NoticeTab">
                        内容设置
                    </div>
                    <form action="" method="post" accept-charset="UTF-8" class="form-horizontal"
                          enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="fields-group">
                                @include('layout.partOne')
                                <div class="form-group" id="" data-id="sort">
                                    <label for="cell" class="col-sm-2 control-label">排序</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            @if('create' == $act)
                                                <input type="number" id="sort" name="sort" style="width: 500px" value=""
                                                       min="0" step="1"
                                                       class="form-control sort"
                                                       placeholder="序号越大，排序越靠前；相同序号的，创建时间越近越靠前">
                                            @else
                                                <input type="number" id="sort" name="sort" style="width: 500px"
                                                       min="0" step="1"
                                                       @if(isset($data['sort']))
                                                       value="{{ $data['sort'] }}"
                                                       @else
                                                       value=""
                                                       @endif
                                                       class="form-control sort"
                                                       placeholder="序号越大，排序越靠前；相同序号的，创建时间越近越靠前">
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-2  control-label">推荐</label>
                                    <div class="col-sm-8 marTop05">
                                        <div id="content-type">
                                            @if('create' == $act)
                                                <label><input type="radio" name="is_recommend" id="" value="1"
                                                              checked="checked">
                                                    &nbsp;推荐</label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <label><input type="radio" name="is_recommend" id="" value="2">
                                                    &nbsp;不推荐</label>
                                            @else
                                                <label><input type="radio" name="is_recommend" id="" value="1"
                                                              @if(isset($data['is_recommend']) && 1 == intval($data['is_recommend'])) checked="checked" @endif>
                                                    &nbsp;推荐</label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <label><input type="radio" name="is_recommend" id="" value="2"
                                                              @if(isset($data['is_recommend']) && 2 == intval($data['is_recommend'])) checked="checked" @endif>
                                                    &nbsp;不推荐</label>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="cateDiv" data-id="cate">
                                    <label for="cell" class="col-sm-2 control-label">专题位</label>
                                    <div class="col-sm-8 marTop05">
                                        <div class="input-group">
                                            @if('create' == $act)
                                                @foreach($topicsArr as $val)
                                                    <label><input type="radio" name="cate" value="{{ $val['id'] }}">
                                                        &nbsp;{{ $val['name'] }}</label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                @endforeach
                                            @else
                                                @foreach($topicsArr as $val)
                                                    <label><input type="radio" name="cate" value="{{ $val['id'] }}"
                                                                  @if(isset($data['cate_id']) && intval($val['id']) == intval($data['cate_id']))
                                                                  checked="checked"
                                                                @endif>
                                                        &nbsp;{{ $val['name'] }}</label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @include('layout.partTwo')
                            </div>

                        </div>
                        <!-- /.box-body -->

                        @include('layout.footer')
                    </form>
                </div>
            </div>
        </div>
    </div>

    @if($act == 'create')
        <input type="hidden" name="tagsStr" id="" value="">
    @else
        <input type="hidden" name="tagsStr" id=""
               @if(isset($data['tags']))
               value="{{ $data['tags'] }}"
               @else
               value=""
                @endif
        >
    @endif
    <input type="hidden" name="content_type_editor" id=""
           @if(isset($data['content_type']))
           value="{{ $data['content_type'] }}"
           @else
           value="1"
            @endif
    >
</section>

<script type="text/javascript" src="{{ URL::asset('/js/layout/layout.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/banners/index.js') }}"></script>