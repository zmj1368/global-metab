<link rel="stylesheet" href="{{ URL::asset('/vendor/bootstrap-table/dist/bootstrap-table.min.css') }}">
<style>
    .inline {
        display: inline;
        margin-right: 10px;
    }
</style>
{{ csrf_field() }}
<section class="invoice">
    <div class="row">
        <div class="col-xs-12">
            <div class="form-horizontal form-inline form-group">
                <div class="inline">
                    <label class="control-label" style="vertical-align: top;">标题: </label>&nbsp;&nbsp;
                    <input type="text" value="" placeholder="请输入标题" class="form-control searchTitle"
                           id="searchTitle">
                </div>
                <div class="inline">
                    <label class="control-label" style="vertical-align: top;">专题位: </label>&nbsp;&nbsp;
                    <select class="form-control filter_input_sm" id="searchCate" title="">
                        <option value="0">请选择</option>
                        @foreach($topicsArr as $val)
                            <option value="{{ $val['id'] }}">{{ $val['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="inline">
                    <label class="control-label" style="vertical-align: top;">是否推荐专题位: </label>&nbsp;&nbsp;
                    <select class="form-control filter_input_sm" id="searchRecommend" title="">
                        <option value="0">请选择</option>
                        <option value="1">推荐</option>
                        <option value="2">不推荐</option>
                    </select>
                </div>
                <div class="inline">
                    <a class="btn btn-info" role="button" data-type="1" id="search">查询</a>
                    <a class="btn btn-default" role="button" data-type="0" id="reset">清空查询</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-bottom: 10px">
        <div class="col-sm-12">
            <a class="btn btn-info pull-right" role="button" href="{{ route($host . '.create') }}"><span
                        class="fa fa-plus-circle"></span>&nbsp;&nbsp;添加</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <table id="table" class="table table-striped" style="word-break:break-all; word-wrap:break-all;"></table>
        </div>
    </div>

    <input type="hidden" id="listUrl" value="{{ route($host . '.list') }}">
    <input type="hidden" id="delUrl" value="{{ route($host . '.del') }}">
</section>
@include('layout.deleteConfirm')
<script src="{{ URL::asset('/vendor/bootstrap-table/dist/bootstrap-table.min.js') }}"></script>
<script src="{{ URL::asset('/js/banners/index.js') }}"></script>
