<style>
    .star {
        color: #FF3300;
        margin-left: 10px;
        line-height: 34px;
        position: absolute;
    }
</style>
<div class="form-group">
    <label for="cell" class="col-sm-2 control-label">标题</label>
    <div class="col-sm-8">
        <div class="input-group">
            @if('create' == $act)
                <input type="text" id="title" name="title" style="width: 500px" value=""
                       class="form-control title" placeholder="标题">
            @else
                <input type="text" id="title" name="title" style="width: 500px"
                       class="form-control title"
                       @if(isset($data['title']))
                       value="{{ $data['title'] }}"
                       @else
                       value=""
                       @endif placeholder="标题">
            @endif
            <span class="star">★</span>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="name" class="col-sm-2  control-label">标签</label>
    <div class="col-sm-8">
        <div class="input-group marTop05">
            <select name="join_tags[]" class="form-control" multiple="multiple" id="tags_list">
                @foreach($tagsArr as $val)
                    <option value="{{$val['id']}}">{{$val['name']}}</option>
                @endforeach
            </select>
            <span class="star">★</span>
        </div>
    </div>
</div>

<div class="form-group" data-id="cover">
    <label for="" class="control-label col-md-2">图片</label>
    <div class="col-md-8 marTop05">
        <div>
            <input type="hidden" name="origin_image_url" value="">
            <input type="hidden" id="new_image" name="image"
                   @if('edit' == $act && $data['image']) value="{{ $data['image'] }}" @else value="" @endif >
            <img id="doc_photo"
                 src="@if('edit' == $act && $data['image']) {{ $data['image'] }}@else {{ App\Models\Ads::DEFAULT_PHOTO }}@endif"
                 class="image_size" alt="背景图片" width="150" height="100" data-width="300"
                 data-height="200">
            <span class="star">★</span>
        </div>
        <div class="marTop1">
            <input type="file" class="hidden" name="image_file"
                   id="image_file" value="浏览..."
                   accept="image/gif,image/png,image/jpeg,image/jpg,image/bmp">
            <a class="btn btn-info" id="upload">上传</a><span class="margin">上传尺寸比例为：高：宽=2:3；图片大小限制在2M内；图片格式支持：jpg、png、jpeg</span>
        </div>
    </div>
</div>


@if('news' != $host)
    <div class="form-group">
        <label for="cell" class="col-sm-2 control-label">作者</label>
        <div class="col-sm-8">
            <div class="input-group">
                @if('create' == $act)
                    <input type="text" id="author" name="author" style="width: 500px"
                           value="" class="form-control author" placeholder="多位作者请用空格隔开">
                @else
                    <input type="text" id="author" name="author" style="width: 500px"
                           @if(isset($data['author']))
                           value="{{ $data['author'] }}"
                           @else
                           value=""
                           @endif
                           class="form-control author" placeholder="多位作者请用空格隔开">
                @endif

            </div>
        </div>
    </div>
@endif

<input type="hidden" name="uploadPhotoUrl" id="" value='{{ route('upload.image') }}'>
<input type="hidden" name="editId" id=""
       @if('create' == $act)
       value='0'
       @else
       value='{{ $data['id'] }}'
        @endif>
<div id="showImg" class="modal fade my-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-body">
            <img width="380" height="260">
        </div>
    </div>
</div>
