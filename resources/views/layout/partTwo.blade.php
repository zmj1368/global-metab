<style>
    .star {
        color: #FF3300;
        margin-left: 10px;
        line-height: 34px;
        position: absolute;
    }

    .editorPlaceholder {
        color: gray;
    }
</style>
<link rel="stylesheet"
      href="{{ URL::asset('vendor/wangEditor-3.1.1/release/fullscreen/wangEditor-fullscreen-plugin.css') }}">

@if('banner' != $host)
    <div class="form-group">
        <label for="name" class="col-sm-2  control-label">状态</label>
        <div class="col-sm-8 marTop05">
            <div id="content-type">
                @if('create' == $act)
                    <label><input type="radio" name="status" id="" value="1" checked="checked"> &nbsp;显示</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <label><input type="radio" name="status" id="" value="0"> &nbsp;隐藏</label>
                @else
                    <label><input type="radio" name="status" id="" value="1"
                                  @if(isset($data['status']) && 1 == intval($data['status'])) checked="checked" @endif>
                        &nbsp;显示</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <label><input type="radio" name="status" id="" value="0"
                                  @if(isset($data['status']) && 0 == intval($data['status'])) checked="checked" @endif>
                        &nbsp;隐藏</label>
                @endif
            </div>
        </div>
    </div>
@endif

<div class="form-group">
    <label for="cell" class="col-sm-2 control-label">来源</label>
    <div class="col-sm-8">
        <div class="input-group">
            @if('create' == $act)
                <input type="text" id="from" name="from" style="width: 500px" value=""
                       class="form-control from" placeholder="不填默认为代谢网">
            @else
                <input type="text" id="from" name="from" style="width: 500px"
                       @if(isset($data['from']))
                       value="{{ $data['from'] }}"
                       @else
                       value=""
                       @endif
                       class="form-control from" placeholder="不填默认为代谢网">
            @endif
        </div>
    </div>
</div>

<div class="form-group" data-id="subject">
    <label for="name" class="col-sm-2  control-label">摘要</label>
    <div class="col-sm-8">
        @if('create' == $act)
            <textarea name="subject" id="subject" cols="20" rows="10" class="form-control" placeholder="最多64个字"
                      maxlength="64"></textarea>
        @else
            <textarea name="subject" id="subject" cols="20" rows="10" class="form-control" placeholder="最多64个字"
                      maxlength="64">@if(isset($data['subject'])){{ $data['subject'] }}@endif</textarea>
        @endif
    </div>
    <span class="star">★</span>
</div>

<div class="form-group">
    <label for="name" class="col-sm-2  control-label">内容</label>
    <div class="col-sm-8 marTop05">
        <div id="content-type">
            <label><input type="radio" name="content_type_id" id="" value="1" checked="checked"> &nbsp;编辑文本</label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label><input type="radio" name="content_type_id" id="" value="2"> &nbsp;使用链接</label>
        </div>
        <div id="contentEditor" class="marTop1">
            <div id="editor">
            </div>
            <div class="editorPlaceholder">
                支持图文、视频；图片格式支持：jpg、png、jpeg；<br>
                视频大小在500M以内; 视频格式支持avi、mp4、3gp、MPEG、MPG、DAT、mov、rmvb、rm、FLV
            </div>
            <textarea name="contentText" style="display: none" id="contentText" cols="30" rows="10"
                      class="form-control"></textarea>
        </div>
        <div id="contentLink" class="marTop1" style="display: none;">
            <input type="text" id="" name="contentLink" value="" class="form-control title" placeholder="链接(必填)">
        </div>
    </div>
</div>

@if('edit' == $act)
    <textarea class="hidden" id="content"
              name="content">{{ empty($data['content']) ? '' : $data['content'] }}</textarea>
@endif
<input type="hidden" name="isEditor" id="" @if('edit' == $act) value="1" @else value="0" @endif>
<input type="hidden" name="noticeEditor" id="" value="{{ route('editor.upload.image') }}">
<input type="hidden" name="noticeVideoEditor" id="" value="{{ route('editor.upload.video') }}">
<script type="text/javascript" src="{{ URL::asset('vendor/wangEditor-3.1.1/release/wangEditor.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('vendor/layer/layer.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('vendor/wangEditor-3.1.1/release/fullscreen/wangEditor-fullscreen-plugin.js') }}"></script>
