<input type="hidden" name="noticeStore" id="" value="{{ route($host . '.store') }}">
@if('edit' == $act)
    <input type="hidden" name="noticeUpdate" id="" value="{{ route($host . '.update', $data['id']) }}">
@endif
<input type="hidden" name="noticeSucRedirect" id="" value="{{ route($host . '.index') }}">
@if('create' == $act)
    <div class="box-footer">
        <div class="col-md-4">
        </div>
        <div class="col-md-1">
            <div class="btn-group pull-left">
                {{--<button type="submit" class="btn btn-primary">提交</button>--}}
                <button type="button" class="btn btn-info pull-right" id="btn_submit">创&nbsp;建
                </button>
            </div>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-2">
            <div class="btn-group pull-left">
                <button type="reset" class="btn btn-warning">撤&nbsp;销</button>
            </div>
        </div>
    </div>
@endif
@if('edit' == $act)
    <div class="box-footer">
        <div class="col-md-4">
        </div>
        <div class="col-md-1">
            <div class="btn-group pull-left">
                {{--<button type="submit" class="btn btn-primary">提交</button>--}}
                <button type="button" class="btn btn-primary pull-right" id="btn_editor">提&nbsp;交
                </button>
            </div>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-2">
            <div class="btn-group pull-left">
                <button type="reset" class="btn btn-warning">撤&nbsp;销</button>
            </div>
        </div>
    </div>
@endif