<?php

if (!function_exists('upload_image')) {
    /**
     * 上传图片到oss服务器
     * @param \Illuminate\Http\UploadedFile $file
     * @param string $root
     * @param array $config
     * @return array
     */
    function upload_image(\Illuminate\Http\UploadedFile $file, $root = '', $config = [])
    {
        if (empty($file)) {
            return ['code' => 1, 'msg' => '文件上传失败'];
        }
        $config = empty($config) ? config('image.image') : $config;
        $root = empty($root) ? config('image.root') : $root;
        $size = $config['size'] ?? 2 * 1024 * 1024;
        $width = $config['width'] ?? '';
        $height = $config['height'] ?? '';
        $extra = empty($config['extra']) ? ['jpg', 'png', 'gif', 'jpeg'] : $config['extra'];

        $ext = strtolower($file->getClientOriginalExtension());

        if (!in_array($ext, $extra)) {
            return ['code' => 2, 'msg' => '请上传合法图片文件'];
        }

        if ($file->getClientSize() > $size) {
            return ['code' => 3, 'msg' => '上传图片大小过大'];
        }

        $date = date('Ym');
        $originType = 'origin';
        $pathUrl = $config['path'] . DIRECTORY_SEPARATOR . $originType . DIRECTORY_SEPARATOR . $date . DIRECTORY_SEPARATOR;
        $path = $root . $pathUrl;
        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }
        $filename = md5(time() . $file->getClientOriginalName()) . '.' . $ext;
        $file->move($path, $filename);

        $resizeType = $width . '_' . $height;
        $resizeUrl = $config['path'] . DIRECTORY_SEPARATOR . $resizeType . DIRECTORY_SEPARATOR . $date . DIRECTORY_SEPARATOR;
        $resizePath = $root . $resizeUrl;
        if (!is_dir($resizePath)) {
            mkdir($resizePath, 0755, true);
        }
        \Intervention\Image\Facades\Image::make($path . $filename)->resize($width, $height)->save($resizePath . $filename);

        $originPath = http_build_query(['name' => $filename, 'type' => $originType, 'date' => $date]);
        $resizePath = http_build_query(['name' => $filename, 'type' => $resizeType, 'date' => $date]);
        return ['code' => 0, 'msg' => 'success', 'data' => ['originPath' => $originPath, 'resizePath' => $resizePath]];
    }
}

if (!function_exists('upload_video')) {
    /**
     * 上传视频到oss服务器
     * @param \Illuminate\Http\UploadedFile $file
     * @param string $root
     * @param array $config
     * @return array
     */
    function upload_video(\Illuminate\Http\UploadedFile $file, $root = '', $config = [])
    {
        if (empty($file)) {
            return ['code' => 1, 'msg' => '视频上传失败'];
        }

        $config = empty($config) ? config('video.video') : $config;
        $root = empty($root) ? config('video.root') : $root;
        $size = $config['size'] ?? 500 * 1024 * 1024;
        $extra = empty($config['extra']) ? ['mp4', 'avi', 'wmv', 'mov'] : $config['extra'];

        $ext = strtolower($file->getClientOriginalExtension());

        if (!in_array($ext, $extra)) {
            return ['code' => 2, 'msg' => '请上传所支持格式后缀名视频'];
        }

        if ($file->getClientSize() > $size) {
            return ['code' => 3, 'msg' => '上传视频大小不能超过500M'];
        }

        $date = date('Ymd');
        $pathUrl = $config['path'] . DIRECTORY_SEPARATOR . $date . DIRECTORY_SEPARATOR;
        $path = $root . $pathUrl;
        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }
        $filename = md5(time() . $file->getClientOriginalName()) . '.' . $ext;
        $file->move($path, $filename);

        $originPath = $pathUrl . $filename;
        return ['code' => 0, 'msg' => 'success', 'data' => ['originPath' => $originPath]];
    }
}

if (!function_exists('delDir')) {
    /**
     * 删除一个路径下的所有文件夹和文件
     * @param $dir
     * @return bool
     */
    function delDir($dir)
    {
        // 判断目标文件夹是否存在
        if (!is_dir($dir)) {
            return true;

        }
        // 先删除目录下的文件：
        $dh = opendir($dir);
        while ($file = readdir($dh)) {
            if ($file != "." && $file != "..") {
                $fullPath = $dir . "/" . $file;
                if (!is_dir($fullPath)) {
                    unlink($fullPath);
                } else {
                    delDir($fullPath);
                }
            }
        }

        closedir($dh);
        // 删除当前文件夹：
        if (rmdir($dir)) {
            return true;
        } else {
            return false;
        }
    }
}