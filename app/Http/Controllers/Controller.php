<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * 后台上传其他站点可用图片地址处理
     * @param integer $id       相关文章ID
     * @param string $imageUrl  图片在 admin-center 中的地址
     * @return string $res      可根据该地址唯一确认图片
     */
    public function updateOtherSiteImageUrl($id, $imageUrl) {
        $path = parse_url(route('upload.image'));
        $img = parse_url($imageUrl);
        $res = $imageUrl;
        if ($img['host'] == $path['host'] && $img['path'] == $path['path']) {
            parse_str($img['query'], $query);
            $query['id'] = $id;
            $res = http_build_query($query);
        }
        return $res;
    }

    /**
     * 后台上传其他站点文章中的图片处理
     * @param integer $id       相关文章ID
     * @param string $content   文章内容
     * @return string $res      文章内容
     */
    public function updateOtherSiteArticleImage($id, $content) {
        preg_match_all('/src=[\'\"]?(http[s]?:\/\/[^\'\"]*)[\'\"]?/', $content, $img);
        if (!empty($img[1])) {
            foreach ($img[1] as $value) {
                $tmpImg = $this->updateOtherSiteImageUrl($id, html_entity_decode($value));
                $content = str_replace($value, $tmpImg, $content);
            }
        }
        return $content;
    }

    /**
     * 获取图片
     * @param $imageUrl
     * @return string
     */
    public function getOtherSiteImageUrl($imageUrl) {
        $path = route('upload.image');
        $img = parse_url($imageUrl);
        $res = $imageUrl;
        if (empty($img['host']) || empty($img['path'])) {
            $res = $path . '?' . $imageUrl;
        }
        return $res;
    }

    /**
     * 替换文章中的图片
     * @param $content
     * @return mixed
     */
    public function getOtherSiteArticleImage($content) {
        preg_match_all('/src=[\'\"]?([^\'\"]*)[\'\"]?/', $content, $img);
        if (!empty($img[1])) {
            foreach ($img[1] as $value) {
                $tmpImg = $this->getOtherSiteImageUrl($value);
                $content = str_replace($value, $tmpImg, $content);
            }
        }
        return $content;
    }
}
