<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix' => config('admin.route.prefix'),
    'namespace' => config('admin.route.namespace'),
    'middleware' => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    $router->get('upload/image', 'ImageController@getImage')->name('upload.image');
    $router->post('upload/image', 'ImageController@upload')->name('upload.image');
    $router->post('editor/upload/image', 'ImageController@editorUpload')->name('editor.upload.image');
    // 上传视频
    $router->post('editor/upload/video', 'VideoController@editorUpload')->name('editor.upload.video');

    //日历管理
    $router->resource('calendars', 'CalendarsController', ['except' => [
        'show', 'destroy'
    ]]);
    Route::group(['prefix' => 'calendars'], function (Router $router) {
        // 获取日历列表
        $router->post('list', 'CalendarsController@getList')->name('calendars.list');
        // 假删除日历
        $router->post('del', 'CalendarsController@delete')->name('calendars.del');
    });

    // 广告位管理
    $router->resource('ads', 'AdsController', ['except' => [
        'show', 'destroy'
    ]]);
    Route::group(['prefix' => 'ads'], function (Router $router) {
        // 获取广告列表
        $router->post('list', 'AdsController@getList')->name('ads.list');
        // 删除广告
        $router->post('del', 'AdsController@delete')->name('ads.del');
    });

    // 简史、未来管理
    $router->resource('history', 'HistoriesController', ['except' => [
        'show', 'destroy'
    ]]);
    Route::group(['prefix' => 'history'], function (Router $router) {
        // 获取广告列表
        $router->post('list', 'HistoriesController@getList')->name('history.list');
        // 删除广告
        $router->post('del', 'HistoriesController@delete')->name('history.del');
    });

    // 首页专题位
    $router->resource('banner', 'BannersController', ['except' => [
        'show', 'destroy'
    ]]);
    Route::group(['prefix' => 'banner'], function (Router $router) {
        // 获取广告列表
        $router->post('list', 'BannersController@getList')->name('banner.list');
        // 删除广告
        $router->post('del', 'BannersController@delete')->name('banner.del');
    });

    // 资讯管理
    $router->resource('news', 'NewsController', ['except' => [
        'show', 'destroy'
    ]]);
    Route::group(['prefix' => 'news'], function (Router $router) {
        // 获取资讯列表
        $router->post('list', 'NewsController@getList')->name('news.list');
        // 删除资讯
        $router->post('del', 'NewsController@delete')->name('news.del');
    });

    // 医学期刊管理
    $router->resource('journalArticle', 'JournalArticleController', ['except' => [
        'show', 'destroy'
    ]]);
    Route::group(['prefix' => 'journalArticle'], function (Router $router) {
        // 获取医学期刊文章列表
        $router->post('list', 'JournalArticleController@getList')->name('journalArticle.list');
        // 删除医学期刊文章
        $router->post('del', 'JournalArticleController@delete')->name('journalArticle.del');
    });

    // 会议追踪管理
    $router->resource('meeting', 'MeetingController', ['except' => [
        'show', 'destroy'
    ]]);
    Route::group(['prefix' => 'meeting'], function (Router $router) {
        // 获取会议追踪列表
        $router->post('list', 'MeetingController@getList')->name('meeting.list');
        // 删除会议追踪
        $router->post('del', 'MeetingController@delete')->name('meeting.del');
    });

    // 专家管理
    $router->resource('experts', 'ExpertsController', ['except' => [
        'show', 'destroy'
    ]]);
    Route::group(['prefix' => 'experts'], function (Router $router) {
        // 获取专家列表数据
        $router->post('list', 'ExpertsController@getList')->name('experts.list');
        // 删除专家
        $router->post('del', 'ExpertsController@delete')->name('experts.del');
    });

    // MMC管理
    $router->resource('mmc', 'MmcController', ['except' => [
        'show', 'destroy'
    ]]);
    Route::group(['prefix' => 'mmc'], function (Router $router) {
        // 获取MMC模块列表
        $router->post('list', 'MmcController@getList')->name('mmc.list');
        // 删除MMC
        $router->post('del', 'MmcController@delete')->name('mmc.del');
    });

    // 在线课堂管理
    $router->resource('course', 'CourseController', ['except' => [
        'show', 'destroy'
    ]]);
    Route::group(['prefix' => 'course'], function (Router $router) {
        // 获取在线课堂模块列表
        $router->post('list', 'CourseController@getList')->name('course.list');
        // 删除在线课堂
        $router->post('del', 'CourseController@delete')->name('course.del');
    });


});
