<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Calendars;
use App\Services\CalendarsService;
use App\Services\TagsService;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CalendarsController extends Controller
{
    private $host = '';

    public $tagsService;
    public $calendarsService;

    public function __construct(TagsService $tagsService, CalendarsService $calendarsService)
    {
        $this->tagsService = $tagsService;
        $this->calendarsService = $calendarsService;
        $this->host = 'calendars';
    }

    /**
     * 首页面列表
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('日历管理 - 日历列表');
            $content->breadcrumb(
                ['text' => '日历管理', 'url' => '/admin/calendars'],
                ['text' => '日历列表', 'url' => '/admin/calendars/index']
            );

            $content->row(view('admin.calendars.index'));
        });
    }

    /**
     * 创建日历
     * @param Request $request
     * @return Content
     */
    public function create(Request $request)
    {
        // 获取标签
        $tagsArr = $this->tagsService->getTags();
        // 获取上一张背景图
        $lastImageUrl = $this->calendarsService->getLastBackGroundImage();

        return Admin::content(function (Content $content) use ($request, $lastImageUrl, $tagsArr) {
            $content->header('日历管理 - 创建日历');
            $content->breadcrumb(
                ['text' => '日历管理', 'url' => '/admin/calendars'],
                ['text' => '新增日历', 'url' => '/admin/calendars/create']
            );

            $view = view('admin.calendars.create', [
                'tagsArr' => $tagsArr,
                'lastImageUrl' => $lastImageUrl,
                'host' => $this->host,
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 获取日历列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $pageSize = intval($request->post('size')) ?? Calendars::PAGE_SIZE;
        $keyWords = trim((string)$request->post('title')) ?? '';
        $arr = $this->calendarsService->getCalendarsList($keyWords, $pageSize);
        return response()->json($arr);
    }

    /**
     * 删除日历数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $id = intval($request->post('id')) ?? 0;
        if (!$id) {
            return response()->json(['code' => 1, 'msg' => '删除有误,请联系管理人员']);
        }
        $res = $this->calendarsService->deleteData($id);
        if (!$res) {
            return response()->json(['code' => 1, 'msg' => '删除出错,请刷新当前页面重新删除']);
        } else {
            return response()->json(['code' => 0, 'msg' => '删除日历成功']);
        }
    }

    /**
     * 编辑日历数据
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('日历管理 - 编辑日历');
            $content->breadcrumb(
                ['text' => '日历管理', 'url' => '/admin/calendars'],
                ['text' => '编辑日历', 'url' => '/admin/calendars/edit']
            );

            // 获取标签
            $tagsArr = $this->tagsService->getTags();
            // 获取数据
            $data = $this->calendarsService->getDataById($id);
            if (empty($data)) {
                admin_toastr('查询参数有误', 'error');
                return header('Location:' . route('calendars.index'));
            }

            $view = view('admin.calendars.edit', ['tagsArr' => $tagsArr, 'data' => $data, 'host' => $this->host]);
            $content->row($view);
        });
    }

    /**
     * 更新日历数据
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     * @throws \OSS\Core\OssException
     */
    public function update($id, Request $request)
    {
        // 判断用户是否有权查看
        $user = Admin::user();
        $params = $request->all();
        $id = intval($id);
        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        // 验证是否有该数据
        $isExist = $this->calendarsService->isExistData($id);
        if (!$isExist) {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
        $res = $this->calendarsService->updateData($id, $params, $user['id']);
        if ($res) {
            return response()->json(['code' => 0, 'msg' => '数据更新成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
    }

    /**
     * 保存数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \OSS\Core\OssException
     */
    public function store(Request $request)
    {
        $user = Admin::user();
        $params = $request->all();
        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        $id = $this->calendarsService->insertDataGetId($params, $user['id']);
        if ($id) {
            return response()->json(['code' => 0, 'msg' => '数据提交成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据提交失败!', 'data' => []]);
        }
    }

    /**
     * 验证所有提交的参数
     * @param $params
     * @return mixed
     */
    private function validatorParams($params)
    {
        $validator = Validator::make($params, Calendars::VALIDATE_RULE, Calendars::messages());
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $message = $errors[0] ?? '数据不合法';
            return ['code' => 2, 'msg' => $message];
        }
        return ['code' => 1];
    }
}