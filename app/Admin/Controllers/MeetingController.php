<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Meetings;
use App\Services\MeetingCatesService;
use App\Services\TagsService;
use App\Services\MeetingService;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MeetingController extends Controller
{
    public $tagsService;
    public $meetingService;
    public $meetingCatesService;
    public $host;

    public function __construct(TagsService $tagsService, MeetingService $meetingService, MeetingCatesService $meetingCatesService)
    {
        $this->host = 'meeting';
        $this->tagsService = $tagsService;
        $this->meetingService = $meetingService;
        $this->meetingCatesService = $meetingCatesService;
    }


    /**
     * 首页面列表
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('会议追踪管理 - 会议追踪列表');
            $content->breadcrumb(
                ['text' => '会议追踪管理', 'url' => '/admin/meeting'],
                ['text' => '会议追踪列表', 'url' => '/admin/meeting/index']
            );

            $tagsArr = $this->tagsService->getTags();
            $meetingsArr = $this->meetingCatesService->getMeetingCates();
            $view = view('admin.meeting.index', [
                'act' => 'index',
                'host' => $this->host,
                'tagsArr' => $tagsArr,
                'meetingsArr' => $meetingsArr
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 创建数据
     * @param Request $request
     * @return Content
     */
    public function create(Request $request)
    {
        return Admin::content(function (Content $content) use ($request) {
            $content->header('会议追踪管理 - 创建会议');
            $content->breadcrumb(
                ['text' => '会议追踪管理', 'url' => '/admin/meeting'],
                ['text' => '新建会议追踪', 'url' => '/admin/meeting/create']
            );
            $meetingsArr = $this->meetingCatesService->getMeetingCates();
            $tagsArr = $this->tagsService->getTags();
            $view = view('admin.meeting.create', [
                'act' => 'create',
                'host' => $this->host,
                'tagsArr' => $tagsArr,
                'meetingsArr' => $meetingsArr
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 获取列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $pageSize = intval($request->post('size')) ?? Meetings::PAGE_SIZE;
        $keyWords = $request->all();
        $arr = $this->meetingService->getList($keyWords, $pageSize);
        return response()->json($arr);
    }

    /**
     * 删除数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $id = intval($request->post('id')) ?? 0;
        if (!$id) {
            return response()->json(['code' => 1, 'msg' => '删除有误,请联系管理人员']);
        }
        $res = $this->meetingService->deleteData($id);
        if (!$res) {
            return response()->json(['code' => 1, 'msg' => '删除出错,请刷新当前页面重新删除']);
        } else {
            return response()->json(['code' => 0, 'msg' => '删除数据成功']);
        }
    }

    /**
     * 编辑会议文章
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('会议管理 - 编辑会议');
            $content->breadcrumb(
                ['text' => '会议追踪管理', 'url' => '/admin/meeting'],
                ['text' => '编辑会议追踪', 'url' => '/admin/meeting/edit']
            );

            // 获取数据
            $data = $this->meetingService->getDataById($id);
            $meetingsArr = $this->meetingCatesService->getMeetingCates();
            $tagsArr = $this->tagsService->getTags();
            if (empty($data)) {
                admin_toastr('查询参数有误', 'error');
                return header('Location:' . route($this->host . '.index'));
            }

            $view = view('admin.meeting.create', [
                'act' => 'edit',
                'host' => $this->host,
                'data' => $data,
                'tagsArr' => $tagsArr,
                'meetingsArr' => $meetingsArr
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 更新会议文章
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     * @throws \OSS\Core\OssException
     */
    public function update($id, Request $request)
    {
        // 判断用户是否有权查看
        $user = Admin::user();
        $params = $request->all();
        $id = intval($id);

        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        // 验证是否有该数据
        $isExist = $this->meetingService->isExistData($id);
        if (!$isExist) {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
        $res = $this->meetingService->updateData($id, $params, $user['id']);
        if ($res) {
            return response()->json(['code' => 0, 'msg' => '数据更新成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
    }

    /**
     * 保存数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \OSS\Core\OssException
     */
    public function store(Request $request)
    {
        $user = Admin::user();
        $params = $request->all();
        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        $id = $this->meetingService->insertDataGetId($params, $user['id']);
        if ($id) {
            return response()->json(['code' => 0, 'msg' => '数据提交成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据提交失败!', 'data' => []]);
        }
    }

    /**
     * 验证所有提交的参数
     * @param $params
     * @return mixed
     */
    private function validatorParams($params)
    {
        $validator = Validator::make($params, Meetings::VALIDATE_RULE, Meetings::messages());
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $message = $errors[0] ?? '数据不合法';
            return ['code' => 2, 'msg' => $message];
        }
        return ['code' => 1];
    }
}