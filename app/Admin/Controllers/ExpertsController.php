<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Experts;
use App\Services\ExpertsService;

use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ExpertsController extends Controller
{
    private $host = '';

    public $expertsService;

    public function __construct(ExpertsService $expertsService)
    {
        $this->expertsService = $expertsService;
        $this->host = 'experts';
    }

    /**
     * 专家管理页面列表
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('专家管理 - 专家列表');
            $content->breadcrumb(
                ['text' => '专家管理', 'url' => '/admin/experts'],
                ['text' => '专家列表', 'url' => '/admin/experts/index']
            );

            $content->row(view('admin.experts.index'));
        });
    }

    /**
     * 创建专家
     * @param Request $request
     * @return Content
     */
    public function create(Request $request)
    {
        return Admin::content(function (Content $content) use ($request) {
            $content->header('专家管理 - 创建专家');
            $content->breadcrumb(
                ['text' => '专家管理', 'url' => '/admin/experts'],
                ['text' => '新建专家', 'url' => '/admin/experts/create']
            );


            $view = view('admin.experts.create', [
                'host' => $this->host,
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 获取专家列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $pageSize = intval($request->post('size')) ?? Experts::PAGE_SIZE;
        $keyWords = $request->all();
        $arr = $this->expertsService->getExpertsList($keyWords, $pageSize);
        return response()->json($arr);
    }

    public function show($id)
    {

    }


    /**
     * 删除专家数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $id = intval($request->post('id')) ?? 0;
        if (!$id) {
            return response()->json(['code' => 1, 'msg' => '删除有误,请联系管理人员']);
        }
        $res = $this->expertsService->deleteData($id);
        if (!$res) {
            return response()->json(['code' => 1, 'msg' => '删除出错,请刷新当前页面重新删除']);
        } else {
            return response()->json(['code' => 0, 'msg' => '删除专家成功']);
        }
    }




    /**
     * 编辑专家数据
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('专家管理 - 编辑专家');

            $content->breadcrumb(
                ['text' => '专家管理', 'url' => '/admin/experts'],
                ['text' => '编辑专家', 'url' => '/admin/experts/edit']
            );


            // 获取数据
            $data = $this->expertsService->getDataById($id);

            if (empty($data)) {
                admin_toastr('查询参数有误', 'error');
                return header('Location:' . route('experts.index'));
            }
            $view = view('admin.experts.edit', ['data' => $data, 'host' => $this->host]);
            $content->row($view);
        });

    }

    /**
     * 更新专家数据
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     * @throws \OSS\Core\OssException
     */
    public function update($id, Request $request)
    {
        // 判断用户是否有权查看
        $user = Admin::user();
        $params = $request->all();
        $id = intval($id);
        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        // 验证是否有该数据
        $isExist = $this->expertsService->isExistData($id);
        if (!$isExist) {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
        $res = $this->expertsService->updateData($id, $params, $user['id']);
        if ($res) {
            return response()->json(['code' => 0, 'msg' => '数据更新成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
    }


    /**
     * 保存专家
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \OSS\Core\OssException
     */
    public function store(Request $request)
    {
        $user = Admin::user();

        $params = $request->all();

        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        $id = $this->expertsService->insertDataGetId($params, $user['id']);
        if ($id) {
            return response()->json(['code' => 0, 'msg' => '数据提交成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据提交失败!', 'data' => []]);
        }


    }

    /**
     * 验证所有提交的参数
     * @param $params
     * @return mixed
     */
    private function validatorParams($params)
    {
        $validator = Validator::make($params, Experts::VALIDATE_RULE, Experts::messages());
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $message = $errors[0] ?? '数据不合法';
            return ['code' => 2, 'msg' => $message];
        }
        return ['code' => 1];
    }
}