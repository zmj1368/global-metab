<?php

namespace App\Admin\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\OssService;
use Illuminate\Support\Facades\Log;

class VideoController extends Controller
{
    /**
     * 编辑器 wangEditor 上传视频（需要固定的返回格式）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editorUpload(Request $request)
    {
        $file = $request->file('video');
        if ($file->isValid()) {
            $res = upload_video($file);
            if ($res['code']) {
                return response()->json(['errno' => $res['code'], 'msg' => $res['msg'], 'data' => []]);
            } else {
                return response()->json(['errno' => 0, 'msg' => 'success', 'data' => [$res['data']['originPath']]]);
            }
        }
        return response()->json(['errno' => 500, 'msg' => '视频上传失败', 'data' => []]);
    }

    /**
     * 上传视频到阿里云
     * @param $videoPath
     * @return bool|string
     * @throws \OSS\Core\OssException
     */
    public static function uploadVideoToOss($videoPath)
    {
        $ossPath = config('video.oss.path');
        $ossService = OssService::getInstance();
        $paths = pathinfo($videoPath);
        $ossImageName = $ossPath . '/' . date('Ym') . '/' . $paths['basename'];

        $pubPath = public_path() . $videoPath;
        if (!file_exists($pubPath)) {
            return '';
        }

        // 类似 zz-med-pub.oss-cn-hangzhou.aliyuncs.com/user/doc/news/20544/logo1453425053.jpeg
        $ossUrl = $ossService->uploadDocPhoto($ossImageName, $pubPath);
        if (!$ossUrl) {
            Log::Warn('管家-系统消息 图片保存到OSS服务器失败', ['path' => $pubPath]);
            return '';
        } else {
            // 上传成功时,删除服务器临时文件
            if (file_exists($pubPath)) {
                unlink($pubPath);
            }
        }
        $ossUrl = self::filterUrl($ossUrl);
        return $ossUrl;
    }

    /**
     * 简单处理下url，http:|https:开头替换掉
     * @param string $url
     * @return string
     */
    private static function filterUrl($url = '')
    {
        $url = trim(preg_replace('/^(http:)|(https:)/', '', trim(strtolower($url))));
        if ($url == '') {
            return '';
        } else {
            return $url;
        }
    }
}
