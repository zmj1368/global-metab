<?php

namespace App\Admin\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Services\OssService;
use Illuminate\Support\Facades\Log;

class ImageController extends Controller
{
    /**
     * 上传图片
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        $file = $request->file('file');

        if ($file->isValid()) {
            return upload_image($file);
        }

        return response()->json(['code' => 100, 'msg' => '图片上传失败']);
    }

    /**
     * 编辑器 wangeditor 上传图片（需要固定的返回格式）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editorUpload(Request $request)
    {
        $file = $request->file('file');
        if ($file->isValid()) {
            $res = upload_image($file);
            if ($res['code']) {
                return response()->json(['errno' => $res['code'], 'msg' => $res['msg'], 'data' => []]);
            } else {
                $url = $res['data']['originPath'];
                return response()->json(['errno' => 0, 'msg' => 'success', 'data' => [route('upload.image', $url)]]);
            }
        }
        return response()->json(['errno' => 500, 'msg' => '图片上传失败', 'data' => []]);
    }

    /**
     * 获取图片
     * @return \Illuminate\Http\Response
     */
    public function getImage()
    {
        $root = empty($root) ? config('image.root') : $root;
        $config = config('image.image');
        $config['path'] = $config['path'] ?? '';
        $basePath = $root . $config['path'];

        $param = \request()->all();
        if (empty($param['type']) || empty($param['date']) || empty($param['name'])) {
            $filePath = '';
        } else {
            $type = $param['type'] ?? $param['type'];
            $date = $param['date'] ?? $param['date'];
            $name = $param['name'] ?? $param['name'];

            $filePath = $basePath . DIRECTORY_SEPARATOR . $type . DIRECTORY_SEPARATOR . $date . DIRECTORY_SEPARATOR . $name;
        }
        if (!$filePath || !file_exists($filePath)) {
            $filePath = public_path('images/no_pic/no_pic.jpg');
        }

        $image = Image::make($filePath);
        $response = response()->make($image->encode('jpg'));
        $response->header('Content-Type', 'image/jpeg');
        return $response;
    }

    /**
     * 判断是否未重新上传的图片
     * @param $url
     * @return bool
     */
    public static function isReUpload($url)
    {
        $p = '/^\/\/zz-med.+/';
        $url = self::filterUrl($url);
        if (preg_match($p, $url)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 内容图片上传到Oss服务器
     * @param $content
     * @param $id
     * @param $ossPath
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    public static function getContentImageLocalPath($content, $id, $ossPath)
    {
        $controller = new Controller();
        preg_match_all('/img\ssrc=[\'\"]?(http[s]?:\/\/[^\'\"]*)[\'\"]?/', $content, $img);
        if (!empty($img[1])) {
            foreach ($img[1] as $value) {
                $imgUrl = $controller->updateOtherSiteImageUrl($id, html_entity_decode($value));
                if ($imgUrl != html_entity_decode($value)) {
                    $tmpImg = self::uploadPhoto($controller->updateOtherSiteImageUrl($id, html_entity_decode($value)), $ossPath);
                    $content = str_replace($value, $tmpImg, $content);
                }
            }
        }

        preg_match_all('/video\ssrc=[\'\"]?([^\'\"]*)[\'\"]?/', $content, $video);
        if (!empty($video[1])) {
            foreach ($video[1] as $value) {
                if (self::isReUpload($value)) {
                    $tmpImg = (new VideoController())->uploadVideoToOss($value);
                    $content = str_replace($value, $tmpImg, $content);
                }
            }
        }

        return $content;
    }

    /**
     * 上传图片到阿里云
     * @param $url
     * @param $ossPath
     * @return bool|string
     * @throws \OSS\Core\OssException
     */
    public static function uploadPhoto($url, $ossPath)
    {
        $ossService = OssService::getInstance();
        $arr = self::getPhotoLocalPath($url);
        $imagePath = $arr['file_path'];
        $ossImageName = $ossPath . '/' . $arr['date'] . '/' . $arr['name'];
        if (!file_exists($imagePath)) {
            return '';
        }
        // 类似 zz-med-pub.oss-cn-hangzhou.aliyuncs.com/user/doc/news/20544/logo1453425053.jpeg
        $ossUrl = $ossService->uploadDocPhoto($ossImageName, $imagePath);
        if (!$ossUrl) {
            Log::Warn('管家-系统消息 图片保存到OSS服务器失败', ['path' => $imagePath]);
            return '';
        } else {
            // 上传成功时,删除服务器临时文件
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
        }
        $ossUrl = self::filterUrl($ossUrl);
        return $ossUrl;
    }

    /**
     * 简单处理下url，http:|https:开头替换掉
     * @param string $url
     * @return string
     */
    private static function filterUrl($url = '')
    {
        $url = trim(preg_replace('/^(http:)|(https:)/', '', trim(strtolower($url))));
        if ($url == '') {
            return '';
        } else {
            return $url;
        }
    }

    /**
     * 获取图片本服务器地址
     * @param $fileName
     * @return array
     */
    public static function getPhotoLocalPath($fileName)
    {
        $tmp_arr = explode('&', $fileName);
        $arr = [];
        foreach ($tmp_arr as $val) {
            $tmp = explode('=', $val);
            $arr[$tmp[0]] = $tmp[1];
        }
        // local path
        $root = empty($root) ? config('image.root') : $root;
        $config = config('image.image');
        $config['path'] = $config['path'] ?? '';
        $basePath = $root . $config['path'];

        $localPath = $basePath . DIRECTORY_SEPARATOR . $arr['type'] . DIRECTORY_SEPARATOR . $arr['date'] . DIRECTORY_SEPARATOR . $arr['name'];
        $arr['file_path'] = $localPath;
        return $arr;
    }
}
