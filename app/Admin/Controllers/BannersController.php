<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TopicArticles;
use App\Services\TagsService;
use App\Services\TopicArticlesService;
use App\Services\TopicsService;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BannersController extends Controller
{
    public $tagsService;
    public $topicsService;
    public $topicArticlesService;
    public $host;

    public function __construct(TagsService $tagsService, TopicsService $topicsService, TopicArticlesService $topicArticlesService)
    {
        $this->host = 'banner';
        $this->tagsService = $tagsService;
        $this->topicsService = $topicsService;
        $this->topicArticlesService = $topicArticlesService;
    }

    /**
     * 首页面列表
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('首页专题位 - 首页专题位列表');
            $content->breadcrumb(
                ['text' => '首页专题位管理', 'url' => '/admin/banner'],
                ['text' => '首页专题位列表', 'url' => '/admin/banner/index']
            );

            $tagsArr = $this->tagsService->getTags();
            $topicsArr = $this->topicsService->getTopics();
            $view = view('admin.banners.index', [
                'act' => 'index',
                'host' => $this->host,
                'tagsArr' => $tagsArr,
                'topicsArr' => $topicsArr
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 创建数据
     * @param Request $request
     * @return Content
     */
    public function create(Request $request)
    {
        return Admin::content(function (Content $content) use ($request) {
            $content->header('首页专题位 - 创建首页专题位');
            $content->breadcrumb(
                ['text' => '首页专题位管理', 'url' => '/admin/banner'],
                ['text' => '新建首页专题位', 'url' => '/admin/banner/create']
            );

            $tagsArr = $this->tagsService->getTags();
            $topicsArr = $this->topicsService->getTopics();
            $view = view('admin.banners.create', [
                'act' => 'create',
                'host' => $this->host,
                'tagsArr' => $tagsArr,
                'topicsArr' => $topicsArr
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 获取列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $pageSize = intval($request->post('size')) ?? TopicArticles::PAGE_SIZE;
        $keyWords = $request->all();
        $arr = $this->topicArticlesService->getList($keyWords, $pageSize);
        return response()->json($arr);
    }

    /**
     * 删除数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $id = intval($request->post('id')) ?? 0;
        if (!$id) {
            return response()->json(['code' => 1, 'msg' => '删除有误,请联系管理人员']);
        }
        $res = $this->topicArticlesService->deleteData($id);
        if (!$res) {
            return response()->json(['code' => 1, 'msg' => '删除出错,请刷新当前页面重新删除']);
        } else {
            return response()->json(['code' => 0, 'msg' => '删除数据成功']);
        }
    }

    /**
     * 编辑广告数据
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('首页专题位管理 - 编辑首页专题位');
            $content->breadcrumb(
                ['text' => '首页专题位管理', 'url' => '/admin/banner'],
                ['text' => '编辑首页专题位', 'url' => '/admin/banner/edit']
            );

            // 获取数据
            $data = $this->topicArticlesService->getDataById($id);
            $tagsArr = $this->tagsService->getTags();
            $topicsArr = $this->topicsService->getTopics();
            if (empty($data)) {
                admin_toastr('查询参数有误', 'error');
                return header('Location:' . route($this->host . '.index'));
            }

            $view = view('admin.banners.create', [
                'act' => 'edit',
                'host' => $this->host,
                'data' => $data,
                'tagsArr' => $tagsArr,
                'topicsArr' => $topicsArr
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 更新日历数据
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     * @throws \OSS\Core\OssException
     */
    public function update($id, Request $request)
    {
        // 判断用户是否有权查看
        $user = Admin::user();
        $params = $request->all();
        $id = intval($id);
        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        // 验证是否有该数据
        $isExist = $this->topicArticlesService->isExistData($id);
        if (!$isExist) {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
        $topicsValidator = $this->topicsService->isExists($params['is_recommend'], $params['cate_id']);
        if (!$topicsValidator) {
            return response()->json(['code' => 1, 'msg' => '该专题位不存在!', 'data' => []]);
        }
        $res = $this->topicArticlesService->updateData($id, $params, $user['id']);
        if ($res) {
            return response()->json(['code' => 0, 'msg' => '数据更新成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
    }

    /**
     * 保存数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \OSS\Core\OssException
     */
    public function store(Request $request)
    {
        $user = Admin::user();
        $params = $request->all();
        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        $topicsValidator = $this->topicsService->isExists($params['is_recommend'], $params['cate_id']);
        if (!$topicsValidator) {
            return response()->json(['code' => 1, 'msg' => '该专题位不存在!', 'data' => []]);
        }
        $id = $this->topicArticlesService->insertDataGetId($params, $user['id']);
        if ($id) {
            return response()->json(['code' => 0, 'msg' => '数据提交成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据提交失败!', 'data' => []]);
        }
    }

    /**
     * 验证所有提交的参数
     * @param $params
     * @return mixed
     */
    private function validatorParams($params)
    {
        $validator = Validator::make($params, TopicArticles::VALIDATE_RULE, TopicArticles::messages());
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $message = $errors[0] ?? '数据不合法';
            return ['code' => 2, 'msg' => $message];
        }
        return ['code' => 1];
    }
}