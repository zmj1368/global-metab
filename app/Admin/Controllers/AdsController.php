<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Ads;
use App\Services\AdsService;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdsController extends Controller
{
    public $adsService;

    public function __construct(AdsService $adsService)
    {
        $this->adsService = $adsService;
    }

    /**
     * 首页面列表
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('广告位管理 - 广告列表');
            $content->breadcrumb(
                ['text' => '广告管理', 'url' => '/admin/ads'],
                ['text' => '广告列表', 'url' => '/admin/ads/index']
            );

            $content->row(view('admin.ads.index'));
        });
    }

    /**
     * 创建日历
     * @param Request $request
     * @return Content
     */
    public function create(Request $request)
    {
        return Admin::content(function (Content $content) use ($request) {
            $content->header('广告位管理 - 创建广告');
            $content->breadcrumb(
                ['text' => '广告管理', 'url' => '/admin'],
                ['text' => '新建广告', 'url' => '/admin/ads/create']
            );
            $view = view('admin.ads.create', [
                'act' => 'create'
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 获取日历列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $pageSize = intval($request->post('size')) ?? Ads::PAGE_SIZE;
        $keyWords = $request->all();
        $arr = $this->adsService->getAdsList($keyWords, $pageSize);
        return response()->json($arr);
    }

    /**
     * 删除广告数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $id = intval($request->post('id')) ?? 0;
        if (!$id) {
            return response()->json(['code' => 1, 'msg' => '删除有误,请联系管理人员']);
        }
        $res = $this->adsService->deleteData($id);
        if (!$res) {
            return response()->json(['code' => 1, 'msg' => '删除出错,请刷新当前页面重新删除']);
        } else {
            return response()->json(['code' => 0, 'msg' => '删除广告成功']);
        }
    }

    /**
     * 编辑广告数据
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('广告位管理 - 广告列表');
            $content->description('编辑');
            $content->breadcrumb(
                ['text' => '广告管理', 'url' => '/admin'],
                ['text' => '编辑广告', 'url' => '/admin/ads/edit']
            );

            // 获取数据
            $data = $this->adsService->getDataById($id);
            if (empty($data)) {
                admin_toastr('查询参数有误', 'error');
                return header('Location:' . route('calendars.index'));
            }

            $view = view('admin.ads.create', [
                'act' => 'edit',
                'data' => $data
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 更新日历数据
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     * @throws \OSS\Core\OssException
     */
    public function update($id, Request $request)
    {
        // 判断用户是否有权查看
        $user = Admin::user();
        $params = $request->all();
        $id = intval($id);
        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        // 验证是否有该数据
        $isExist = $this->adsService->isExistData($id);
        if (!$isExist) {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
        $res = $this->adsService->updateData($id, $params, $user['id']);
        if ($res) {
            return response()->json(['code' => 0, 'msg' => '数据更新成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
    }

    /**
     * 保存数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \OSS\Core\OssException
     */
    public function store(Request $request)
    {
        $user = Admin::user();
        $params = $request->all();
        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        $id = $this->adsService->insertDataGetId($params, $user['id']);
        if ($id) {
            return response()->json(['code' => 0, 'msg' => '数据提交成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据提交失败!', 'data' => []]);
        }
    }

    /**
     * 验证所有提交的参数
     * @param $params
     * @return mixed
     */
    private function validatorParams($params)
    {
        $validator = Validator::make($params, Ads::VALIDATE_RULE, Ads::messages());
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $message = $errors[0] ?? '数据不合法';
            return ['code' => 2, 'msg' => $message];
        }
        return ['code' => 1];
    }
}