<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Histories;
use App\Services\HistoriesService;
use App\Services\TagsService;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HistoriesController extends Controller
{
    public $tagsService;
    public $historiesService;
    public $host;

    public function __construct(TagsService $tagsService, HistoriesService $historiesService)
    {
        $this->host = 'history';
        $this->tagsService = $tagsService;
        $this->historiesService = $historiesService;
    }

    /**
     * 首页面列表
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('简史+未来管理 - 简史+未来列表');
            $content->breadcrumb(
                ['text' => '简史+未来管理', 'url' => '/admin/histories'],
                ['text' => '简史+未来管理列表', 'url' => '/admin/histories/index']
            );


            $tagsArr = $this->tagsService->getTags();
            $view = view('admin.histories.index', [
                'act' => 'index',
                'host' => $this->host,
                'tagsArr' => $tagsArr
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 创建数据
     * @param Request $request
     * @return Content
     */
    public function create(Request $request)
    {
        return Admin::content(function (Content $content) use ($request) {
            $content->header('简史+未来管理 - 创建简史+未来');
            $content->breadcrumb(
                ['text' => '简史+未来管理', 'url' => '/admin/histories'],
                ['text' => '简史+未来管理列表', 'url' => '/admin/histories/create']
            );

            $tagsArr = $this->tagsService->getTags();
            $view = view('admin.histories.create', [
                'act' => 'create',
                'host' => $this->host,
                'tagsArr' => $tagsArr
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 获取列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $pageSize = intval($request->post('size')) ?? Histories::PAGE_SIZE;
        $keyWords = $request->all();
        $arr = $this->historiesService->getList($keyWords, $pageSize);
        return response()->json($arr);
    }

    /**
     * 删除数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $id = intval($request->post('id')) ?? 0;
        if (!$id) {
            return response()->json(['code' => 1, 'msg' => '删除有误,请联系管理人员']);
        }
        $res = $this->historiesService->deleteData($id);
        if (!$res) {
            return response()->json(['code' => 1, 'msg' => '删除出错,请刷新当前页面重新删除']);
        } else {
            return response()->json(['code' => 0, 'msg' => '删除数据成功']);
        }
    }

    /**
     * 编辑广告数据
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('简史+未来管理 - 编辑简史+未来');

            $content->breadcrumb(
                ['text' => '简史+未来管理', 'url' => '/admin/histories'],
                ['text' => '编辑简史+未来管理', 'url' => '/admin/histories/edit']
            );

            // 获取数据
            $data = $this->historiesService->getDataById($id);
            $tagsArr = $this->tagsService->getTags();
            if (empty($data)) {
                admin_toastr('查询参数有误', 'error');
                return header('Location:' . route($this->host . '.index'));
            }

            $view = view('admin.histories.create', [
                'act' => 'edit',
                'host' => $this->host,
                'data' => $data,
                'tagsArr' => $tagsArr
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 更新日历数据
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     * @throws \OSS\Core\OssException
     */
    public function update($id, Request $request)
    {
        // 判断用户是否有权查看
        $user = Admin::user();
        $params = $request->all();
        $id = intval($id);
        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        // 验证是否有该数据
        $isExist = $this->historiesService->isExistData($id);
        if (!$isExist) {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
        $res = $this->historiesService->updateData($id, $params, $user['id']);
        if ($res) {
            return response()->json(['code' => 0, 'msg' => '数据更新成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
    }

    /**
     * 保存数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \OSS\Core\OssException
     */
    public function store(Request $request)
    {
        $user = Admin::user();
        $params = $request->all();
        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        $id = $this->historiesService->insertDataGetId($params, $user['id']);
        if ($id) {
            return response()->json(['code' => 0, 'msg' => '数据提交成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据提交失败!', 'data' => []]);
        }
    }

    /**
     * 验证所有提交的参数
     * @param $params
     * @return mixed
     */
    private function validatorParams($params)
    {
        $validator = Validator::make($params, Histories::VALIDATE_RULE, Histories::messages());
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $message = $errors[0] ?? '数据不合法';
            return ['code' => 2, 'msg' => $message];
        }
        return ['code' => 1];
    }
}