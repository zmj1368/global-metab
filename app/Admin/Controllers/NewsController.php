<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Services\ExpertsService;
use App\Services\NewsService;
use App\Services\NewsCatesService;
use App\Services\TagsService;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    public $tagsService;
    public $newsService;
    public $newsCatesService;
    public $expertsService;
    public $host;

    public function __construct(TagsService $tagsService, NewsService $newsService, NewsCatesService $newsCatesService, ExpertsService $expertsService)
    {
        $this->host = 'news';
        $this->tagsService = $tagsService;
        $this->newsService = $newsService;
        $this->newsCatesService = $newsCatesService;
        $this->expertsService = $expertsService;
    }

    /**
     * 首页面列表
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('资讯管理 - 资讯列表');
            $content->description('列表');

            $newsArr = $this->newsCatesService->getNewsCates();

            $tagsArr = $this->tagsService->getTags();
            $view = view('admin.news.index', [
                'act' => 'index',
                'host' => 'news',
                'tagsArr' => $tagsArr,
                'newsArr' => $newsArr
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 创建资讯
     * @param Request $request
     * @return Content
     */
    public function create(Request $request)
    {
        return Admin::content(function (Content $content) use ($request) {
            $content->header('资讯管理 - 创建资讯');
            $content->description('新建');


            $newsArr = $this->newsCatesService->getNewsCates();

            // 获取 作者 列表
            $authorArr = $this->expertsService->getExpertsName();

            $tagsArr = $this->tagsService->getTags();
            $view = view('admin.news.create', [
                'act' => 'create',
                'host' => 'news',
                'tagsArr' => $tagsArr,
                'newsArr' => $newsArr,
                'authorArr' => $authorArr
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 获取资讯列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $pageSize = intval($request->post('size')) ?? News::PAGE_SIZE;
        $keyWords = $request->all();
        $arr = $this->newsService->getNewsList($keyWords, $pageSize);
        return response()->json($arr);
    }

    /**
     * 删除资讯数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $id = intval($request->post('id')) ?? 0;
        if (!$id) {
            return response()->json(['code' => 1, 'msg' => '删除有误,请联系管理人员']);
        }
        $res = $this->newsService->deleteData($id);
        if (!$res) {
            return response()->json(['code' => 1, 'msg' => '删除出错,请刷新当前页面重新删除']);
        } else {
            return response()->json(['code' => 0, 'msg' => '删除广告成功']);
        }
    }

    /**
     * 编辑资讯数据
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('资讯管理 - 资讯列表');
            $content->description('编辑');
            $tagsArr = $this->tagsService->getTags();
            $newsArr = $this->newsCatesService->getNewsCates();

            // 获取 作者 列表
            $authorArr = $this->expertsService->getExpertsName();
            // 获取数据
            $data = $this->newsService->getDataById($id);
            if (empty($data)) {
                admin_toastr('查询参数有误', 'error');
                return header('Location:' . route('news.index'));
            }

            $view = view('admin.news.create', [
                'act' => 'edit',
                'tagsArr' => $tagsArr,
                'host' => $this->host,
                'data' => $data,
                'newsArr' => $newsArr,
                'authorArr' => $authorArr
            ])->render();
            $content->row($view);
        });
    }

    /**
     * 更新资讯数据
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \OSS\Core\OssException
     */
    public function update($id, Request $request)
    {
        // 判断用户是否有权查看
        $user = Admin::user();
        $params = $request->all();
        $id = intval($id);
        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        // 验证是否有该数据
        $isExist = $this->newsService->isExistData($id);
        if (!$isExist) {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
        // 验证专家解读情况下作者数据
        $res = $this->expertsService->checkExpertsData($params['cate_id'], $params['author']);
        if (!$res) {
            return response()->json(['code' => 1, 'msg' => '作者数据有误,请刷新当前页面重新填写!', 'data' => []]);
        }
        $res = $this->newsService->updateData($id, $params, $user['id']);
        if ($res) {
            return response()->json(['code' => 0, 'msg' => '数据更新成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据更新失败!', 'data' => []]);
        }
    }

    /**
     * 保存数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \OSS\Core\OssException
     */
    public function store(Request $request)
    {
        $user = Admin::user();
        $params = $request->all();
        // 验证所有的参数
        $validator = $this->validatorParams($params);
        if ($validator['code'] == 2) {
            return response()->json($validator);
        }
        // 验证专家解读情况下作者数据
        $res = $this->expertsService->checkExpertsData($params['cate_id'], $params['author']);
        if (!$res) {
            return response()->json(['code' => 1, 'msg' => '作者数据有误,请刷新当前页面重新填写!', 'data' => []]);
        }
        $id = $this->newsService->insertDataGetId($params, $user['id']);
        if ($id) {
            return response()->json(['code' => 0, 'msg' => '数据提交成功!', 'data' => []]);
        } else {
            return response()->json(['code' => 1, 'msg' => '数据提交失败!', 'data' => []]);
        }
    }

    /**
     * 验证所有提交的参数
     * @param $params
     * @return mixed
     */
    private function validatorParams($params)
    {
        $validator = Validator::make($params, News::VALIDATE_RULE, News::messages());
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $message = $errors[0] ?? '数据不合法';
            return ['code' => 2, 'msg' => $message];
        }
        return ['code' => 1];
    }
}