<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calendars extends Model
{
    public $table = 'calendars';

    public $guarded = [];

    const DEFAULT_PHOTO = 'https://mmc.zz-med-stg.com/dist/image/clanda.png';
    const OSS_PATH = 'global/metab/calendars';
    const RESIZE_SIZE = 'x-oss-process=image/resize,m_fixed,w_286,h_325,limit_0';

    const PAGE_SIZE = 20;

    const VALIDATE_RULE = [
        'title' => 'required|string|max:14',
        'image' => '',
        'tags' => 'required|between:1,2',
        'content' => 'required',
    ];

    const NO_DEL = 0; // 未删除
    const IS_DEL = 1; // 删除

    /**
     * 报错信息
     * @return array
     */
    public static function messages()
    {
        return [
            'title.required' => '标题不能为空',
            'title.string' => '标题必须为字符串',
            'title.max' => '标题不能大于10位',
            'tags.required' => '标签必须填写',
            'tags.between' => '标签最多选两个',
            'content.required' => '内容不许填写',
        ];
    }
}