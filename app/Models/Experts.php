<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experts extends Model
{
    public $table = 'experts';

    public $guarded = [];

    const DEFAULT_PHOTO = 'https://mmc.zz-med-stg.com/dist/image/clanda.png';
    const OSS_PATH = 'global/metab/experts';
    const RESIZE_SIZE = 'x-oss-process=image/resize,m_fixed,w_286,h_325,limit_0';

    const PAGE_SIZE = 10;

    const VALIDATE_RULE = [
        'name' => 'required|string|max:10',
        'image' => '',
        'content' => 'required',
    ];

    // 状态
    const SHOW_STATUS = 1;
    const HIDE_STATUS = 0;

    // 专家解读
    const EXPERT_STATUS = 2;


    /**
     * 报错信息
     * @return array
     */
    public static function messages()
    {
        return [
            'name.required' => '专家名字不能为空',
            'name.string' => '名字必须为字符串',
            'name.max' => '标题不能大于10位',
            'content.required' => '内容必须填写',
        ];
    }
}