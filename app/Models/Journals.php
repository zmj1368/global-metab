<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Journals extends Model
{
    public $table = 'journals';

    public $guarded = [];
}