<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    public $table = 'ads';

    public $guarded = [];

    const DEFAULT_PHOTO = 'https://zz-med-pub.oss-cn-hangzhou.aliyuncs.com/shared/images/default/doc_logo.jpg';
    const OSS_PATH = 'global/metab/ads';
    const RESIZE_SIZE = 'x-oss-process=image/resize,m_fixed,w_720,h_300,limit_0';

    const PAGE_SIZE = 20;

    // 广告位置
    // 首页
    const INDEX_SPACE = 1;
    // 会议
    const MEETING_SPACE = 2;
    // 专家解读
    const EXPERT_SPACE = 3;

    const SPACE_NAME = [
        self::INDEX_SPACE => '首页',
        self::MEETING_SPACE => '会议追踪',
        self::EXPERT_SPACE => '专家解读',
    ];

    // 状态
    const SHOW_STATUS = 1;
    const HIDE_STATUS = 0;

    const VALIDATE_RULE = [
        'title' => 'required|string',
        'space' => 'required|array',
        "space.*" => 'required|string|distinct|in:1, 2, 3',
        'image' => 'required',
        'link' => '',
        'sort' => 'required',
        'status' => 'required|in:1,0',
    ];

    /**
     * 报错信息
     * @return array
     */
    public static function messages()
    {
        return [
            'title.required' => '标题不能为空',
            'title.string' => '标题必须为字符串',
            'space.required' => '位置必须填写',
            'space.array' => '位置参数不正确',
            'space.*.in' => '位置参数不正确',
            'space.*.string' => '位置参数不正确',
            'space.*.distinct' => '位置参数不正确',
            'image.required' => '图片必须上传',
            'link.required' => '链接必须填写',
            'status.required' => '状态参数不正确',
            'status.in' => '状态参数不正确',
        ];
    }
}