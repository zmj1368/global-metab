<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $table = 'news';

    public $guarded = [];

    const OSS_PATH = 'global/metab/news';

    const PAGE_SIZE = 20;

    // 分类
    // 代谢简史
    const METABOLISE_CAT = 1;
    // 洞见未来
    const FEATURE_CAT = 2;

    const SPACE_NAME = [
        self::METABOLISE_CAT => '洞见未来',
        self::FEATURE_CAT => '洞见未来',
    ];

    // 状态
    const SHOW_STATUS = 1;
    const HIDE_STATUS = 0;

    // 推荐
    const IS_RECOMMEND = 1;
    // 不推荐
    const NO_RECOMMEND = 2;

    // 正文状态
    // 编辑文本
    const TEXT_CONTENT_TYPE = 1;
    // 使用链接
    const VALIDATE_RULE = [
        'title' => 'required|string',
        'image' => 'required|string',
        "cate_id" => 'required',
        "content_type" => 'required|in:1, 2',
        'tags' => 'required|between:1,2',
        'status' => 'required|in:1,0',
        'author' => '',
        'is_recommend' => 'required|in:1, 0',
        'from' => '',
        'content' => 'required',
        'subject' => 'required|string|max:64',
    ];

    const LINK_CONTENT_TYPE = 2;

    /**
     * 报错信息
     * @return array
     */
    public static function messages()
    {
        return [
            'title.required' => '标题不能为空',
            'title.string' => '标题必须为字符串',
            'image.required' => '图片必须上传',
            'image.string' => '图片链接必须为字符串',
            'cate_id.required' => '分类必须填写',
            'content_type.required' => '正文类型必须上传',
            'content_type.in' => '正文类型参数有误',
            'is_recommend.required' => '是否推荐必须选择',
            'is_recommend.in' => '是否推荐参数有误',
            'tags.required' => '标签必须填写',
            'tags.between' => '标签最多选两个',
            'status.required' => '状态参数不正确',
            'status.in' => '状态参数不正确',
            'subject.required' => '摘要必须填写',
            'subject.string' => '摘要必须为字符串',
            'subject.max' => '摘要不能大于64位',
            'content.required' => '内容必须填写',
        ];
    }
}