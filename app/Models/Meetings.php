<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meetings extends Model
{
    public $table = 'meetings';

    public $guarded = [];

    const OSS_PATH = 'global/metab/meetings/';

    const PAGE_SIZE = 20;

    // 状态
    const SHOW_STATUS = 1;
    const HIDE_STATUS = 0;

    // 正文状态
    // 编辑文本
    const TEXT_CONTENT_TYPE = 1;
    // 使用链接
    const VALIDATE_RULE = [
        'title' => 'required|string',
        'tags' => 'required|between:1,2',
        'image' => 'required|string',
        'author' => '',
//        'sort' => 'integer|min:0',
//        "is_recommend" => 'required|in:1, 2',
        'from' => '',
        'subject' => 'required|string|max:64',
//        "cate_id" => 'required|integer',
        "content_type" => 'required|in:1, 2',
        'content' => 'required',
    ];

    const LINK_CONTENT_TYPE = 2;

    /**
     * 报错信息
     * @return array
     */
    public static function messages()
    {
        return [
            'title.required' => '标题不能为空',
            'title.string' => '标题必须为字符串',
            'tags.required' => '标签必须填写',
            'tags.between' => '标签最多选两个',
            'image.required' => '图片必须上传',
            'image.string' => '图片链接必须为字符串',
            'subject.required' => '摘要必须填写',
            'subject.string' => '摘要必须为字符串',
            'subject.max' => '摘要不能大于64位',
//            'cate_id.required' => '专题位必须填写',
//            'cate_id.integer' => '专题位参数必须为整数',
            'content_type.required' => '正文类型必须上传',
            'content_type.in' => '正文类型参数有误',
            'content.required' => '内容必须填写',
        ];
    }
}