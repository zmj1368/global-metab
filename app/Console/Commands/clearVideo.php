<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class clearVideo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clearVideo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '清除两天前过期的视频文件';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // 删除public目录下两天前上传的视频
        $date = date('Ymd', strtotime('-2 days'));
        // wangEditor视频保存路径
        $videoConfig = config('video.video');
        $path = public_path() . $videoConfig['path'] . '/' . $date . '/';
        delDir($path);
    }
}
