<?php

namespace App\Services;

use App\Models\Journals;

class JournalsService
{
    /**
     * 获取期刊名称
     * @return array
     */
    public function getJournals()
    {
        $journalsArr = Journals::query()->select(['id', 'name'])->get()->toArray();
        return $journalsArr ?? [];
    }
}