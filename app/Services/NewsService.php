<?php

namespace App\Services;

use App\Admin\Controllers\ImageController;
use App\Http\Controllers\Controller;
use App\Models\Experts;
use App\Models\NewsCates;
use App\Models\News;

class NewsService
{
    /**
     * 插入数据库并返回id
     * @param $params
     * @param $operatorId
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    public function insertDataGetId($params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $id = News::query()->insertGetId($data);
        return $id;
    }

    /**
     * 判断数据是否存在
     * @param $id
     * @return bool
     */
    public function isExistData($id)
    {
        $res = News::query()
            ->where('id', '=', $id)
            ->exists();
        return $res;
    }

    /**
     * 删除数据
     * @param $id
     * @return bool
     */
    public function deleteData($id)
    {
        $res = News::query()->where('id', '=', $id)
            ->delete();
        return $res;
    }

    /**
     * 插入数据库并返回id
     * @param $id
     * @param $params
     * @param $operatorId
     * @return int
     * @throws \OSS\Core\OssException
     */
    public function updateData($id, $params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $res = News::query()->where('id', '=', $id)
            ->update($data);
        return $res;
    }

    /**
     * 处理数据
     * @param $params
     * @param $uid
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    private function dealWithData($params, $uid)
    {
        $controller = new Controller();
        // 标签
        $params['tags'] = implode(',', $params['tags']);
        // 图片
        // 判断是否是需要上传的图片
        if (!empty($params['image']) && ImageController::isReUpload($params['image'])) {
            // 上传图片到阿里云服务器并resize
            $params['image'] = ImageController::uploadPhoto($controller->updateOtherSiteImageUrl($uid, $params['image']), News::OSS_PATH);
        }
        // 内容形式
        $params['content_type'] = intval($params['content_type']) ?? News::TEXT_CONTENT_TYPE;

        if (News::TEXT_CONTENT_TYPE == intval($params['content_type'])) {
            // 内容
            $params['content'] = ImageController::getContentImageLocalPath($params['content'], $uid, News::OSS_PATH);
        }
        // 操作者
        $params['operator_id'] = $uid;
        // 来源(没有填则为代谢网)
        $params['from'] = trim($params['from']) != '' ? trim($params['from']) : '代谢网';
        // 分类
        $params['cate_id'] = intval($params['cate_id']) ?? 1;
        // 作者
        if (Experts::EXPERT_STATUS != $params['cate_id']) {
            $params['author'] = trim($params['author']) != '' ? trim($params['author']) : '代谢网';
        } else {
            if ($params['author'] && !empty($params['author'])) {
                $params['author'] = implode(',', $params['author']);
            } else {
                $params['author'] = '';
            }
        }
        // 状态
        $params['status'] = intval($params['status']) ?? 1;
        // 标题
        $params['title'] = $params['title'] ?? '';
        // 摘要
        $params['subject'] = $params['subject'] ?? '';
        // 点击数
        $params['hits'] = 0;
        return $params;
    }

    /**
     * 根据关键字查询资讯列表
     * @param $keyWords
     * @param $pageSize
     * @return array
     */
    public function getNewsList($keyWords, $pageSize)
    {
        $pageSize = intval($pageSize) ?? News::PAGE_SIZE;
        $query = News::query();

//        echo "<pre>";
//        var_dump($keyWords);die;

        if ($keyWords['searchTitle']) {
            $query->where('name', 'like', '%' . $keyWords['searchTitle'] . '%');
        }

        // 状态，是否启用

//        echo "<pre>";
//        var_dump($keyWords['searchStatus']);die;
        if (in_array(intval($keyWords['searchStatus']), [News::SHOW_STATUS, News::HIDE_STATUS])) {
            $query = $query->where("status", '=', intval($keyWords['searchStatus']));
        }

        // 资讯分类搜索
        if (intval($keyWords['searchCate']) > 0) {
            $query = $query->where("cate_id", '=', intval($keyWords['searchCate']));
        }
        // 是否推荐
        if (in_array(intval($keyWords['searchRecommend']), [News::IS_RECOMMEND, News::NO_RECOMMEND])) {
            $query = $query->where("is_recommend", '=', intval($keyWords['searchRecommend']));
        }


        $list = $query
            ->orderByDesc('created_at')
            ->paginate($pageSize);
        $total = $list->total();
        $list = $list->toArray();

        //处理分类
        foreach ($list['data'] as $key => $val) {
            $list['data'][$key]['newsCatesStr'] = $this->getCateNameById(intval($val['cate_id']));
        }


        $data = $list['data'];

        // 处理
        return ['total' => intval($total), 'rows' => $data];

    }


    /**
     * 根据资讯id来获取数据
     * @param $id
     * @return array
     */
    public function getDataById($id)
    {
        $data = News::query()
            ->find($id);
        if (!$data) {
            return [];
        }
        $data = $data->toArray();
        return $data;
    }


    /**
     * 根据cate_id获取分类名称
     * @param $id
     * @return string
     */
    public function getCateNameById($id)
    {
        $name = NewsCates::query()->select(['name'])->find($id);
        $name = $name->toArray();
        return $name['name'] ?? '';
    }


}