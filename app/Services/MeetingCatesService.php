<?php

namespace App\Services;

use App\Models\MeetingCates;

class MeetingCatesService
{
    /**
     * 获取会议类别
     * @return array
     */
    public function getMeetingCates()
    {
        $arr = MeetingCates::query()->select(['id', 'name'])->get()->toArray();
        return $arr ?? [];
    }
}