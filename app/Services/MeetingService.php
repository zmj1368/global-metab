<?php

namespace App\Services;

use App\Admin\Controllers\ImageController;
use App\Http\Controllers\Controller;
use App\Models\Meetings;
use App\Models\MeetingCates;

class MeetingService
{
    /**
     * 插入数据库并返回id
     * @param $params
     * @param $operatorId
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    public function insertDataGetId($params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $id = Meetings::query()->insertGetId($data);
        return $id;
    }

    /**
     * 插入数据库并返回id
     * @param $id
     * @param $params
     * @param $operatorId
     * @return int
     * @throws \OSS\Core\OssException
     */
    public function updateData($id, $params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $res = Meetings::query()->where('id', '=', $id)
            ->update($data);
        return $res;
    }

    /**
     * 删除数据
     * @param $id
     * @return bool
     */
    public function deleteData($id)
    {
        $res = Meetings::query()->where('id', '=', $id)
            ->delete();
        return $res;
    }

    /**
     * 根据会议id来获取数据
     * @param $id
     * @return array
     */
    public function getDataById($id)
    {
        $data = Meetings::query()
            ->find($id);
        if (!$data) {
            return [];
        }
        $data = $data->toArray();
        return $data;
    }

    /**
     * 获取列表
     * @param $keyWords
     * @param $pageSize
     * @return array
     */
    public function getList($keyWords, $pageSize)
    {
        $pageSize = intval($pageSize) ?? Meetings::PAGE_SIZE;
        $query = Meetings::query();
        // 标题
        if ($keyWords['searchTitle']) {
            $query->where('title', 'like', '%' . $keyWords['searchTitle'] . '%');
        }

        // 会议分类

        if (intval($keyWords['searchMeetingCates']) > 0) {
            $query = $query->where("cate_id", '=', intval($keyWords['searchMeetingCates']));
        }


        // 状态
        if (in_array(intval($keyWords['searchStatus']), [Meetings::SHOW_STATUS, Meetings::HIDE_STATUS])) {
            $query = $query->where("status", '=', intval($keyWords['searchStatus']));
        }
        $list = $query->orderByDesc('created_at')
            ->paginate($pageSize);
        $total = $list->total();
        $list = $list->toArray();

        //处理所属期刊
        foreach ($list['data'] as $key => $val) {
            $list['data'][$key]['meetingsStr'] = $this->getCateNameById(intval($val['cate_id']));
        }


        return ['total' => intval($total), 'rows' => $list['data']];
    }

    /**
     * 处理数据
     * @param $params
     * @param $uid
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    private function dealWithData($params, $uid)
    {
        $controller = new Controller();
        // 标签
        $params['tags'] = implode(',', $params['tags']);
        // 图片
        // 判断是否是需要上传的图片
        if (!empty($params['image']) && ImageController::isReUpload($params['image'])) {
            // 上传图片到阿里云服务器并resize
            $params['image'] = ImageController::uploadPhoto($controller->updateOtherSiteImageUrl($uid, $params['image']), Meetings::OSS_PATH);
        }
        // 内容形式
        $params['content_type'] = intval($params['content_type']) ?? Meetings::TEXT_CONTENT_TYPE;

        if (Meetings::TEXT_CONTENT_TYPE == intval($params['content_type'])) {
            // 内容
            $params['content'] = ImageController::getContentImageLocalPath($params['content'], $uid, Meetings::OSS_PATH);
        }
        // 操作者
        $params['operator_id'] = $uid;
        // 来源(没有填则为代谢网)
        $params['from'] = $params['from'] == '' ? '代谢网' : $params['from'];
        // 作者
        $params['author'] = $params['author'] ?? '';
        // 会议分类
//        $params['journals'] = intval($params['journals']) ?? Meeting::METABOLISE_CAT;
        // 状态
        $params['status'] = intval($params['status']) ?? Meetings::SHOW_STATUS;
        // 标题
        $params['title'] = $params['title'] ?? '';
        // 摘要
        $params['subject'] = $params['subject'] ?? '';
        // 点击数
        $params['hits'] = 0;
        return $params;
    }

    /**
     * 判断数据是否存在
     * @param $id
     * @return bool
     */
    public function isExistData($id)
    {
        $res = Meetings::query()
            ->where('id', '=', $id)
            ->exists();
        return $res;
    }

    /**
     * 根据cate_id获取会议类别
     * @param $id
     * @return string
     */
    public function getCateNameById($id)
    {
        $name = MeetingCates::query()->select(['name'])->find($id);
        $name = $name->toArray();
        return $name['name'] ?? '';
    }


}