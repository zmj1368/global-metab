<?php

namespace App\Services;

use App\Models\TopicArticles;
use App\Models\Topics;

class TopicsService
{
    /**
     * 获取专题位
     * @return array
     */
    public function getTopics()
    {
        $arr = Topics::query()->select(['id', 'name'])->get()->toArray();
        return $arr ?? [];
    }

    /**
     * 判断专题位是否存在
     * @param $isRecommend
     * @param $cateId
     * @return bool
     */
    public function isExists($isRecommend, $cateId)
    {
        if (TopicArticles::NO_RECOMMEND == intval($isRecommend)) {
            return true;
        }
        $res = Topics::query()
            ->where('id', '=', $cateId)
            ->exists();
        return $res;
    }
}