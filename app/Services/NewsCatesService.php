<?php

namespace App\Services;

use App\Models\NewsCates;

class NewsCatesService
{
    /**
     * 获取会议类别
     * @return array
     */
    public function getNewsCates()
    {
        $arr = NewsCates::query()->select(['id', 'name'])->get()->toArray();
        return $arr ?? [];
    }
}