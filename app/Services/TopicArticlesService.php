<?php
/**
 * Created by PhpStorm.
 * User: zhumj
 * Date: 2019-03-11
 * Time: 16:59
 */

namespace App\Services;

use App\Admin\Controllers\ImageController;
use App\Http\Controllers\Controller;
use App\Models\Histories;
use App\Models\TopicArticles;
use App\Models\Topics;

class TopicArticlesService
{
    /**
     * 插入数据库并返回id
     * @param $params
     * @param $operatorId
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    public function insertDataGetId($params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $id = TopicArticles::query()->insertGetId($data);
        return $id;
    }

    /**
     * 插入数据库并返回id
     * @param $id
     * @param $params
     * @param $operatorId
     * @return int
     * @throws \OSS\Core\OssException
     */
    public function updateData($id, $params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $res = TopicArticles::query()->where('id', '=', $id)
            ->update($data);
        return $res;
    }

    /**
     * 删除数据
     * @param $id
     * @return bool
     */
    public function deleteData($id)
    {
        $res = TopicArticles::query()->where('id', '=', $id)
            ->delete();
        return $res;
    }

    /**
     * 根据日历id来获取数据
     * @param $id
     * @return array
     */
    public function getDataById($id)
    {
        $data = TopicArticles::query()
            ->find($id);
        if (!$data) {
            return [];
        }
        $data = $data->toArray();
        $data['tagsArr'] = explode(',', $data['tags']);
        return $data;
    }

    /**
     * 获取列表
     * @param $keyWords
     * @param $pageSize
     * @return array
     */
    public function getList($keyWords, $pageSize)
    {
        $pageSize = intval($pageSize) ?? TopicArticles::PAGE_SIZE;
        $query = TopicArticles::query();
        // 标题
        if ($keyWords['searchTitle']) {
            $query->where('title', 'like', '%' . $keyWords['searchTitle'] . '%');
        }
        // 专题位
        if (intval($keyWords['searchCate'])) {
            $query = $query->where("cate_id", '=', intval($keyWords['searchCate']));
        }
        // 是否推荐专题位
        if (in_array(intval($keyWords['searchRecommend']), [TopicArticles::IS_RECOMMEND, TopicArticles::NO_RECOMMEND])) {
            $query = $query->where("is_recommend", '=', intval($keyWords['searchRecommend']));
        }
        $list = $query->orderByDesc('sort')
            ->orderByDesc('created_at')
            ->paginate($pageSize);
        $total = $list->total();
        $list = $list->toArray();

        // 处理专题位
        foreach ($list['data'] as $key => $val) {
            if (TopicArticles::IS_RECOMMEND == intval($val['is_recommend'])) {
                $list['data'][$key]['cateStr'] = $this->getCateNameById(intval($val['cate_id']));
            } else {
                $list['data'][$key]['cateStr'] = '无';
            }
        }

        return ['total' => intval($total), 'rows' => $list['data']];
    }

    /**
     * 处理数据
     * @param $params
     * @param $uid
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    private function dealWithData($params, $uid)
    {
        $controller = new Controller();
        // 标签
        $params['tags'] = implode(',', $params['tags']);
        // 图片
        // 判断是否是需要上传的图片
        if (!empty($params['image']) && ImageController::isReUpload($params['image'])) {
            // 上传图片到阿里云服务器并resize
            $params['image'] = ImageController::uploadPhoto($controller->updateOtherSiteImageUrl($uid, $params['image']), TopicArticles::OSS_PATH);
        }
        // 内容形式
        $params['content_type'] = intval($params['content_type']) ?? TopicArticles::TEXT_CONTENT_TYPE;

        if (TopicArticles::TEXT_CONTENT_TYPE == intval($params['content_type'])) {
            // 内容
            $params['content'] = ImageController::getContentImageLocalPath($params['content'], $uid, TopicArticles::OSS_PATH);
        }
        // 操作者
        $params['operator_id'] = $uid;
        // 来源(没有填则为代谢网)
        $params['from'] = $params['from'] == '' ? '代谢网' : $params['from'];
        // 作者
        $params['author'] = $params['author'] ?? '';
        // 是否推荐
        $params['is_recommend'] = intval($params['is_recommend']) ?? TopicArticles::NO_RECOMMEND;
        // 标题
        $params['title'] = $params['title'] ?? '';
        // 排序
        $params['sort'] = intval($params['sort']) ?? 0;
        // 摘要
        $params['subject'] = $params['subject'] ?? '';
        // 专题
        if (TopicArticles::NO_RECOMMEND == $params['is_recommend']) {
            $params['cate_id'] = 0;
        } else {
            $params['cate_id'] = intval($params['cate_id']) ?? 0;
        }
        // 点击数
        $params['hits'] = 0;
        return $params;
    }

    /**
     * 判断数据是否存在
     * @param $id
     * @return bool
     */
    public function isExistData($id)
    {
        $res = TopicArticles::query()
            ->where('id', '=', $id)
            ->exists();
        return $res;
    }


    /**
     * 根据cate_id获取专题位名称
     * @param $id
     * @return string
     */
    public function getCateNameById($id)
    {
        $name = Topics::query()->select(['name'])->find($id);
        $name = $name->toArray();
        return $name['name'] ?? '';
    }
}