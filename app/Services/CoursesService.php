<?php
/**
 * Created by PhpStorm.
 * User: zhumj
 * Date: 2019-03-11
 * Time: 16:59
 */

namespace App\Services;

use App\Admin\Controllers\ImageController;
use App\Http\Controllers\Controller;
use App\Models\Courses;

class CoursesService
{
    /**
     * 插入数据库并返回id
     * @param $params
     * @param $operatorId
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    public function insertDataGetId($params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $id = Courses::query()->insertGetId($data);
        return $id;
    }

    /**
     * 插入数据库并返回id
     * @param $id
     * @param $params
     * @param $operatorId
     * @return int
     * @throws \OSS\Core\OssException
     */
    public function updateData($id, $params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $res = Courses::query()->where('id', '=', $id)
            ->update($data);
        return $res;
    }

    /**
     * 删除数据
     * @param $id
     * @return bool
     */
    public function deleteData($id)
    {
        $res = Courses::query()->where('id', '=', $id)
            ->delete();
        return $res;
    }

    /**
     * 根据日历id来获取数据
     * @param $id
     * @return array
     */
    public function getDataById($id)
    {
        $data = Courses::query()
            ->find($id);
        if (!$data) {
            return [];
        }
        $data = $data->toArray();
        $data['tagsArr'] = explode(',', $data['tags']);
        return $data;
    }

    /**
     * 获取列表
     * @param $keyWords
     * @param $pageSize
     * @return array
     */
    public function getList($keyWords, $pageSize)
    {
        $pageSize = intval($pageSize) ?? Courses::PAGE_SIZE;
        $query = Courses::query();
        // 标题
        if ($keyWords['searchTitle']) {
            $query->where('title', 'like', '%' . $keyWords['searchTitle'] . '%');
        }
        // 状态
        if (in_array(intval($keyWords['searchStatus']), [Courses::SHOW_STATUS, Courses::HIDE_STATUS])) {
            $query = $query->where("status", '=', intval($keyWords['searchStatus']));
        }
        $list = $query->orderByDesc('created_at')
            ->paginate($pageSize);
        $total = $list->total();
        $list = $list->toArray();

        return ['total' => intval($total), 'rows' => $list['data']];
    }

    /**
     * 处理数据
     * @param $params
     * @param $uid
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    private function dealWithData($params, $uid)
    {
        $controller = new Controller();
        // 标签
        $params['tags'] = implode(',', $params['tags']);
        // 图片
        // 判断是否是需要上传的图片
        if (!empty($params['image']) && ImageController::isReUpload($params['image'])) {
            // 上传图片到阿里云服务器并resize
            $params['image'] = ImageController::uploadPhoto($controller->updateOtherSiteImageUrl($uid, $params['image']), Courses::OSS_PATH);
        }
        // 内容形式
        $params['content_type'] = intval($params['content_type']) ?? Courses::TEXT_CONTENT_TYPE;

        if (Courses::TEXT_CONTENT_TYPE == intval($params['content_type'])) {
            // 内容
            $params['content'] = ImageController::getContentImageLocalPath($params['content'], $uid, Courses::OSS_PATH);
        }
        // 操作者
        $params['operator_id'] = $uid;
        // 来源(没有填则为代谢网)
        $params['from'] = $params['from'] == '' ? '代谢网' : $params['from'];
        // 作者
        $params['author'] = $params['author'] ?? '';
        // 标题
        $params['title'] = $params['title'] ?? '';
        // 状态
        $params['status'] = intval($params['status']) ?? Courses::SHOW_STATUS;
        // 摘要
        $params['subject'] = $params['subject'] ?? '';
        // 点击数
        $params['hits'] = 0;
        return $params;
    }

    /**
     * 判断数据是否存在
     * @param $id
     * @return bool
     */
    public function isExistData($id)
    {
        $res = Courses::query()
            ->where('id', '=', $id)
            ->exists();
        return $res;
    }
}