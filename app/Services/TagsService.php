<?php
/**
 * Created by PhpStorm.
 * User: zhumj
 * Date: 2019-03-11
 * Time: 16:59
 */

namespace App\Services;

use App\Models\Tags;

class TagsService
{
    /**
     * 获取标签
     * @return array
     */
    public function getTags()
    {
        $tagsArr = Tags::query()->select(['id', 'name'])->get()->toArray();
        return $tagsArr ?? [];
    }
}