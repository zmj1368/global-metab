<?php

namespace App\Services;

use App\Admin\Controllers\ImageController;
use App\Http\Controllers\Controller;
use App\Models\Experts;

class ExpertsService
{
    /**
     * 获取最后一次发布的背景图,没有的话则为默认图片
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|string|null
     */
    public function getLastBackGroundImage()
    {
        $imageUrl = Experts::query()->select(['image'])->orderByDesc('created_at')->first();
        if (!$imageUrl) {
            $imageUrl = Experts::DEFAULT_PHOTO;
        } else {
            $imageUrl = trim($imageUrl->image);
        }
        return $imageUrl ?? Experts::DEFAULT_PHOTO;
    }

    /**
     * 插入数据库并返回id
     * @param $params
     * @param $operatorId
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    public function insertDataGetId($params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $id = Experts::query()->insertGetId($data);
        return $id;
    }


    /**
     * 更新数据库并返回id
     * @param $id
     * @param $params
     * @param $operatorId
     * @return int
     * @throws \OSS\Core\OssException
     */
    public function updateData($id, $params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $res = Experts::query()->where('id', '=', $id)
            ->update($data);
        return $res;
    }


    /**
     * 处理数据
     * @param $params
     * @param $uid
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    private function dealWithData($params, $uid)
    {

        $controller = new Controller();
        // 图片
        if (!$params['image'] || Experts::DEFAULT_PHOTO == $params['image']) {
            $params['image'] = Experts::DEFAULT_PHOTO;
        } else {
            // 判断是否是需要上传的图片
            if (ImageController::isReUpload($params['image'])) {
                // 上传图片到阿里云服务器并resize
                $ossRes = ImageController::uploadPhoto($controller->updateOtherSiteImageUrl($uid, $params['image']), Experts::OSS_PATH);
                $params['image'] = $ossRes . '?' . Experts::RESIZE_SIZE;
            }
        }
        // 内容
        $params['content'] = ImageController::getContentImageLocalPath($params['content'], $uid, Experts::OSS_PATH);
        // 操作者
        $params['operator_id'] = $uid;
        return $params;
    }

    /**
     * 根据关键字查询专家列表
     * @param $keyWords
     * @param $pageSize
     * @return array
     */
    public function getExpertsList($keyWords, $pageSize)
    {
        $pageSize = intval($pageSize) ?? Experts::PAGE_SIZE;
        $query = Experts::query();

//        echo "<pre>";
//        var_dump($keyWords);die;

        if ($keyWords['searchTitle']) {
            $query->where('name', 'like', '%' . $keyWords['searchTitle'] . '%');
        }

        // 状态，是否启用

//        echo "<pre>";
//        var_dump($keyWords['searchStatus']);die;
        if (in_array(intval($keyWords['searchStatus']), [Experts::SHOW_STATUS, Experts::HIDE_STATUS])) {
            $query = $query->where("status", '=', intval($keyWords['searchStatus']));
        }

        $list = $query
            ->paginate($pageSize);
        $total = $list->total();
        $list = $list->toArray();

        $data = $list['data'];

        // 处理
        return ['total' => intval($total), 'rows' => $data];

    }


    /**
     * 删除专家数据
     * @param $id
     * @return bool
     */
    public function deleteData($id)
    {
        $res = Experts::query()->where('id', '=', $id)
            ->delete();
        return $res;
    }


    /**
     * 根据专家id来获取数据
     * @param $id
     * @return array
     */
    public function getDataById($id)
    {
        $data = Experts::query()
            ->find($id);
        if (!$data) {
            return [];
        }
        $data = $data->toArray();
        return $data;
    }

    /**
     * 判断数据是否存在
     * @param $id
     * @return bool
     */
    public function isExistData($id)
    {
        $res = Experts::query()
            ->where('id', '=', $id)
            ->exists();
        return $res;
    }

    /**
     * 查作者姓名
     * @return array
     */
    public function getExpertsName()
    {
        $arr = Experts::query()
            ->select(['id', 'name'])
            ->where('status', '=', Experts::SHOW_STATUS)
            ->orderByRaw('CONVERT(name USING gbk)')
            ->get()->toArray();
        return $arr;
    }

    /**
     * 验证当专家解读情况下医生数据是否正确
     * @param $cate
     * @param $arr
     * @return bool
     */
    public function checkExpertsData($cate, $arr)
    {
        if (Experts::EXPERT_STATUS != $cate) {
            return true;
        }
        if (!empty($arr)) {
            // 必须为数组类型
            if (!is_array($arr)) {
                return false;
            }
            // 不空检验数据
            $nums = Experts::query()->whereIn('id', $arr)->count();
            if (count($arr) != $nums) {
                return false;
            }
        }
        return true;
    }
}