<?php
/**
 * Created by PhpStorm.
 * User: zhumj
 * Date: 2019-03-11
 * Time: 16:59
 */

namespace App\Services;

use App\Admin\Controllers\ImageController;
use App\Http\Controllers\Controller;
use App\Models\Calendars;

class CalendarsService
{
    /**
     * 获取最后一次发布的背景图,没有的话则为默认图片
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|string|null
     */
    public function getLastBackGroundImage()
    {
        $imageUrl = Calendars::query()->select(['image'])->orderByDesc('created_at')->first();
        if (!$imageUrl) {
            $imageUrl = Calendars::DEFAULT_PHOTO;
        } else {
            $imageUrl = trim($imageUrl->image);
        }
        return $imageUrl ?? Calendars::DEFAULT_PHOTO;
    }

    /**
     * 插入数据库并返回id
     * @param $params
     * @param $operatorId
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    public function insertDataGetId($params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $id = Calendars::query()->insertGetId($data);
        return $id;
    }

    /**
     * 插入数据库并返回id
     * @param $id
     * @param $params
     * @param $operatorId
     * @return int
     * @throws \OSS\Core\OssException
     */
    public function updateData($id, $params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $res = Calendars::query()->where('id', '=', $id)
            ->update($data);
        return $res;
    }

    /**
     * 判断数据是否存在
     * @param $id
     * @return bool
     */
    public function isExistData($id)
    {
        $res = Calendars::query()
            ->where('id', '=', $id)
            ->where('is_del', '=', Calendars::NO_DEL)
            ->exists();
        return $res;
    }

    /**
     * 处理数据
     * @param $params
     * @param $uid
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    private function dealWithData($params, $uid)
    {
        $controller = new Controller();
        // 标签
        $params['tags'] = implode(',', $params['tags']);
        // 图片
        if (!$params['image'] || Calendars::DEFAULT_PHOTO == $params['image']) {
            $params['image'] = Calendars::DEFAULT_PHOTO;
        } else {
            // 判断是否是需要上传的图片
            if (ImageController::isReUpload($params['image'])) {
                // 上传图片到阿里云服务器并resize
                $ossRes = ImageController::uploadPhoto($controller->updateOtherSiteImageUrl($uid, $params['image']), Calendars::OSS_PATH);
                $params['image'] = $ossRes . '?' . Calendars::RESIZE_SIZE;
            }
        }
        // 内容
        $params['content'] = ImageController::getContentImageLocalPath($params['content'], $uid, Calendars::OSS_PATH);
        // 操作者
        $params['publish_date'] = date('Y-m-d');
        $params['is_del'] = Calendars::NO_DEL;
        $params['operator_id'] = $uid;
        return $params;
    }

    /**
     * 根据关键字查询日历列表
     * @param $keyWords
     * @param $pageSize
     * @return array
     */
    public function getCalendarsList($keyWords, $pageSize)
    {
        $pageSize = intval($pageSize) ?? Calendars::PAGE_SIZE;
        $query = Calendars::query();
        if ('' != $keyWords) {
            $query->where('title', 'like', '%' . $keyWords . '%');
        }
        $list = $query->where('is_del', '=', Calendars::NO_DEL)
            ->orderByDesc('created_at')
            ->paginate($pageSize);
        $total = $list->total();
        $list = $list->toArray();

        return ['total' => intval($total), 'rows' => $list['data']];
    }

    /**
     * 伪删除日历数据
     * @param $id
     * @return bool
     */
    public function deleteData($id)
    {
        $res = Calendars::query()->where('id', '=', $id)
            ->update([
                'is_del' => Calendars::IS_DEL
            ]);
        return $res;
    }

    /**
     * 根据日历id来获取数据
     * @param $id
     * @return array
     */
    public function getDataById($id)
    {
        $data = Calendars::query()
            ->where('is_del', '=', Calendars::NO_DEL)
            ->find($id);
        if (!$data) {
            return [];
        }
        $data = $data->toArray();
        return $data;
    }
}