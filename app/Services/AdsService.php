<?php
/**
 * Created by PhpStorm.
 * User: zhumj
 * Date: 2019-03-11
 * Time: 16:59
 */

namespace App\Services;

use App\Admin\Controllers\ImageController;
use App\Http\Controllers\Controller;
use App\Models\Ads;

class AdsService
{
    /**
     * 插入数据库并返回id
     * @param $params
     * @param $operatorId
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    public function insertDataGetId($params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $id = Ads::query()->insertGetId($data);
        return $id;
    }

    /**
     * 处理数据
     * @param $params
     * @param $uid
     * @return mixed
     * @throws \OSS\Core\OssException
     */
    private function dealWithData($params, $uid)
    {
        $controller = new Controller();
        // 标签
        $params['space'] = implode(',', $params['space']);
        // 图片
        if (!$params['image'] || Ads::DEFAULT_PHOTO == $params['image']) {
            $params['image'] = Ads::DEFAULT_PHOTO;
        } else {
            // 判断是否是需要上传的图片
            if (ImageController::isReUpload($params['image'])) {
                // 上传图片到阿里云服务器并resize
                $ossRes = ImageController::uploadPhoto($controller->updateOtherSiteImageUrl($uid, $params['image']), Ads::OSS_PATH);
                $params['image'] = $ossRes . '?' . Ads::RESIZE_SIZE;
            }
        }
        $params['sort'] = intval($params['sort']) ?? 0;
        $params['status'] = intval($params['status']) ?? 0;
        $params['link'] = $params['link'] ?? '';
        // 操作者
        $params['operator_id'] = $uid;
        return $params;
    }

    /**
     * 返回广告列表
     * @param $keyWords
     * @param $pageSize
     * @return array
     */
    public function getAdsList($keyWords, $pageSize)
    {
        $pageSize = intval($pageSize) ?? Ads::PAGE_SIZE;
        $query = Ads::query();
        if ($keyWords['searchTitle']) {
            $query->where('title', 'like', '%' . $keyWords['searchTitle'] . '%');
        }
        // 位置
        if ('-1' == intval($keyWords['searchSpace'])) {
            // 查询全部位置
            $query->where('space', '=', Ads::INDEX_SPACE . ',' . Ads::MEETING_SPACE . ',' . Ads::EXPERT_SPACE);
        } elseif (in_array(intval($keyWords['searchSpace']), [Ads::INDEX_SPACE, Ads::MEETING_SPACE, Ads::EXPERT_SPACE])) {
            $query = $query->whereRaw("FIND_IN_SET(" . intval($keyWords['searchSpace']) . ", space)");
        }
        // 状态
        if (in_array(intval($keyWords['searchStatus']), [Ads::SHOW_STATUS, Ads::HIDE_STATUS])) {
            $query = $query->where("status", '=', intval($keyWords['searchStatus']));
        }
        $list = $query->orderByDesc('sort')
            ->orderByDesc('created_at')
            ->paginate($pageSize);
        $total = $list->total();
        $list = $list->toArray();

        foreach ($list['data'] as $key => $val) {
            $list['data'][$key]['spaceName'] = $this->dealWithSpace($val);
        }
        return ['total' => intval($total), 'rows' => $list['data']];
    }

    /**
     * 删除广告数据
     * @param $id
     * @return bool
     */
    public function deleteData($id)
    {
        $res = Ads::query()->where('id', '=', $id)
            ->delete();
        return $res;
    }

    /**
     * 根据日历id来获取数据
     * @param $id
     * @return array
     */
    public function getDataById($id)
    {
        $data = Ads::query()
            ->find($id);
        if (!$data) {
            return [];
        }
        $data = $data->toArray();
        $data['spaceArr'] = explode(',', $data['space']);
        return $data;
    }

    /**
     * 处理位置名称
     * @param $data
     * @return string
     */
    public function dealWithSpace($data)
    {
        // 处理位置名称
        $spaceArr = explode(',', $data['space']);
        $tmpArr = [];
        foreach ($spaceArr as $item) {
            $tmpArr[] = Ads::SPACE_NAME[intval($item)];
        }
        return implode(',', $tmpArr) ?? '';
    }

    /**
     * 判断数据是否存在
     * @param $id
     * @return bool
     */
    public function isExistData($id)
    {
        $res = Ads::query()
            ->where('id', '=', $id)
            ->exists();
        return $res;
    }

    /**
     * 插入数据库并返回id
     * @param $id
     * @param $params
     * @param $operatorId
     * @return int
     * @throws \OSS\Core\OssException
     */
    public function updateData($id, $params, $operatorId)
    {
        // 处理数据
        $data = $this->dealWithData($params, $operatorId);
        $res = Ads::query()->where('id', '=', $id)
            ->update($data);
        return $res;
    }
}