<?php

namespace App\Services;
use OSS\OssClient;
use Illuminate\Support\Facades\Log;

class OssService
{
    public $ossClient;
    public function __construct()
    {
        $accessId = env('ALIYUN_OSS_ACCESS_ID','GjmmaXV0zHLLfEes');
        $accessKey = env('ALIYUN_OSS_ACCESS_KEY','pDBFEdd3xsWbjtmE9USxJ7kFtRYLj9');
        $this->ossClient = new OssClient($accessId, $accessKey, 'oss-cn-hangzhou.aliyuncs.com');
    }

    /**
     * 静态成品变量 保存全局实例
     */
    private static $_instance = NULL;

    /**
     * 静态工厂方法，返还此类的唯一实例
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     *   oss 上传图片
     * @param $OssFile
     * @param $localFile
     * @return bool
     * @throws \OSS\Core\OssException
     */
    public function uploadDocPhoto($OssFile,$localFile)
    {
        $result = $this->ossClient->uploadFile(env('STAFF_OSS_BUCKET_PUB','zz-med-test-pub'), $OssFile, $localFile);
        if ($result['oss-redirects'] == 0) {
            return  $result['oss-request-url'];
        } else {
            Log::info('阿里OSS上传错误返回：' . json_encode($result));
            return false;
        }
    }

    /**
     *   oss 上传图片
     * @param $OssFile
     * @param $localFile
     * @return bool
     * @throws \OSS\Core\OssException
     */
    public function uploadSoftWareFile($OssFile,$localFile)
    {
        // 'STAFF_OSS_BUCKET_APP' => 'download-zz-med-test',
        $result = $this->ossClient->uploadFile(env('STAFF_OSS_BUCKET_APP','download-zz-med-stg'), $OssFile, $localFile);
        if ($result['oss-redirects'] == 0) {
            return  $result['oss-request-url'];
        } else {
            Log::info('阿里OSS上传错误返回：' . json_encode($result));
            return false;
        }
    }

    /**
     * 上传上传文件
     * @param $ossFile
     * @return bool
     * @throws \OSS\Core\OssException
     */
    public function deleteSoftwareFile($ossFile)
    {
        $result = $this->ossClient->deleteObject(env('STAFF_OSS_BUCKET_APP','download-zz-med-stg'), $ossFile);
        if ($result['oss-redirects'] == 0) {
            return  $result['oss-request-url'];
        } else {
            Log::info('阿里OSS删除错误返回：' . json_encode($result));
            return false;
        }
    }
}