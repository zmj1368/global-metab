$(function () {

    $('#reset,#search').click(function () {
        if (!$(this).data('type')) {
            $('#searchStatus').val(-1);
            $('.searchTitle').val('')
        }
        getList($(this).data('type'));
    });

    // 默认选中标签
    var originTags = $('input[name=tagsStr]').val();
    // 切割字符串
    if (originTags) {
        var originTagsArr = originTags.split(',');
        $("#tags_list").val(originTagsArr).trigger('change');
    }


    // 点击删除
    $('body').on('click', '.delBtn', function () {
        var id = $.trim($(this).attr('data-id'));
        if (parseInt(id) == 0) {
            toastr.error("该数据有误,请勿操作并联系管理人员")
            return false;
        }

        if (!id) {
            toastr.error('请指定要操作的数据！');
            return false;
        }

        $('.offlineDoc').attr('data-id', id).attr('data-status', 1);
        // 弹出模态框
        $('#modal_offlineDoctor').modal('show');
    });

    // 确认删除
    $('.offlineDoc').click(function () {
        var _this = $(this);
        if (_this.hasClass('btn-disabled')) {
            return false;
        }
        addDisabled(_this);
        var id = $(this).attr('data-id');

        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()}
        });

        $.ajax({
            url: $('#delUrl').val(),
            type: 'POST',
            data: {id: id},
            success: function (result) {
                removeDisabled(_this);
                // 取消模态框
                $('#modal_offlineDoctor').modal('hide');
                if (result.code > 0) {
                    toastr.error(result.msg);
                } else if (result.code == 0) {
                    // 请求通过
                    toastr.success(result.msg);
                    getList(1)
                } else {
                    toastr.error('服务器错误！');
                }
            }
        });
    });

    // 添加btn-disabled
    var addDisabled = function (obj) {
        if (!obj.hasClass('btn-disabled')) {
            obj.addClass('btn-disabled');
        }
    };

    // 移出btn-disabled
    var removeDisabled = function (obj) {
        if (obj.hasClass('btn-disabled')) {
            obj.removeClass('btn-disabled');
        }
    };

    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()}
    });
    $('#table').bootstrapTable({
        columns: [{
            field: 'id',
            title: 'ID',
            align: 'center',
            width: '50px',
        }, {
            field: 'title',
            title: '标题',
            align: 'center',
            width: "200px",
        }, {
            field: 'image',
            title: '封面',
            align: 'center',
            formatter: function (value, row, index) {
                return `<img src="${value}" style="height: 65px;width: 57px">`
            }
        }, {
            field: 'status',
            title: '状态',
            align: 'center',
            width: '100px',
            formatter: function (value, row, index) {
                return value == 1 ? '显示' : '隐藏';
            }
        }, {
            field: 'created_at',
            title: '创建时间',
            align: 'center',
            width: "150px",
        }, {
            title: '操作',
            align: 'center',
            width: '200px',
            formatter: function (value, row, index) {
                return `<a class="detail" href="/admin/mmc/${row.id}/edit">编辑</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="delBtn" href="javascript:void(0);" data-id="${row.id}">删除</a>`
            }
        }],
        pageSize: 20,
        url: $('#listUrl').val(),
        method: 'post',
        dataType: 'json',
        striped: true,
        pagination: true,
        sidePagination: 'server',
        // paginationVAlign: 'top',
        paginationHAlign: 'right',
        formatRecordsPerPage: function (pageNumber) {
            return '每页显示 ' + pageNumber + ' 条数据';
        },
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return '共 ' + totalRows + ' 条数据';
        },
        queryParams: function (params) {
            return {
                searchTitle: $.trim($('#searchTitle').val()) ? $.trim($('#searchTitle').val()) : '',
                searchStatus: $('#searchStatus').val(),
                page: (params.offset / params.limit) + 1,
                size: params.limit
            };
        }
    });

    // 点击更新 提交
    var editFlag = 0;
    $('#btn_editor').click(function () {
        var option = checkPostData();
        if (option == false) {
            return false;
        }
        var noticeStore = $('input[name=noticeUpdate]').val();
        var noticeSucRedirect = $('input[name=noticeSucRedirect]').val();
        if (editFlag == 0) {
            editFlag++;

            // 发送请求
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()}
            });
            $.ajax({
                url: noticeStore,
                type: "PUT",
                data: option,
                success: function (result) {
                    editFlag = 0;
                    if (result.code > 0) {
                        // 请求失败
                        toastr.error(result.msg);
                        window.location.reload();
                    } else if (result.code == 0) {
                        // 请求验证通过
                        toastr.success(result.msg);
                        window.location.href = noticeSucRedirect
                    } else {
                        toastr.error('服务器错误,请重新填写！');
                    }
                },
                error: function (res) {
                    var errors = res.responseJSON.errors;
                    $.each(errors, function (index, value) {
                        $.each(value, function (key, val) {
                            toastr.error(val);
                            console.log(val)
                        })
                    })
                }
            });
        }
    });

    // 提交表单
    var createFlag = 0;

    // 点击创建 提交
    $('#btn_submit').click(function () {

        var option = checkPostData();

        if (option == false) {
            return false;
        }
        var noticeStore = $('input[name=noticeStore]').val();
        var noticeSucRedirect = $('input[name=noticeSucRedirect]').val();
        if (createFlag == 0) {
            // 锁定
            createFlag++;

            // 发送请求
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()}
            });
            $.ajax({
                url: noticeStore,
                type: "POST",
                data: option,
                success: function (result) {
                    createFlag = 0;
                    if (result.code > 0) {
                        // 请求失败
                        toastr.error(result.msg);
                        window.location.reload();
                    } else if (result.code == 0) {
                        // 请求验证通过
                        toastr.success(result.msg);
                        window.location.href = noticeSucRedirect
                    } else {
                        toastr.error('服务器错误,请重新填写！');
                    }
                },
                error: function (res) {
                    createFlag = 0;
                    var errors = res.responseJSON.errors;
                    $.each(errors, function (index, value) {
                        $.each(value, function (key, val) {
                            toastr.error(val);
                            console.log(val)
                        })
                    })
                }
            });
        }
    });
});


function getList(type) {
    var params = $('#table').bootstrapTable('getOptions'), data = {
        page: params.pageNumber,
        size: params.pageSize
    };
    if (type) {
        data = {
            searchTitle: $.trim($('#searchTitle').val()) ? $.trim($('#searchTitle').val()) : '',
            searchStatus: $('#searchStatus').val(),
            page: params.pageNumber,
            size: params.pageSize
        };
    } else {
        data = {
            searchTitle: '',
            searchStatus: -1,
            page: params.pageNumber,
            size: params.pageSize
        }
    }

    $.ajax({
        url: $('#listUrl').val(),
        type: "POST",
        data: data,
        success: function (res) {
            $('#table').bootstrapTable('load', res);
        }
    });
}

// 验证提交数据
function checkPostData() {
    var title = $.trim($('input[name=title]').val());
    if (title == '') {
        toastr.error('标题必填!');
        return false;
    }
    // 标签
    var join_tags = $('#tags_list').val();
    if (join_tags == null) {
        toastr.error('标签必须选择!');
        return false;
    } else if (join_tags.length > 2) {
        toastr.error('标签最多选两个!');
        return false;
    }

    // 图片
    var cover_img = $('input[name=image]').val();
    if (cover_img == "") {
        toastr.error('图片必须上传!');
        return false;
    }

    // 作者
    var author = $('input[name=author]').val();

    // 状态
    var status = parseInt($('input[name=status]:checked').val());
    if (status != 1 && status != 0) {
        toastr.error('状态出错,请刷新当前页面重新提交!');
        return false;
    }

    // 来源
    var from = $('input[name=from]').val();

    // 摘要
    var subject = $('#subject').val();
    if (subject == '') {
        toastr.error('摘要必填!');
        return false;
    }

    // 正文类型
    var content_type = parseInt($('input[name=content_type_id]:checked').val());
    if (content_type != 1 && content_type != 2) {
        toastr.error('正文类型出错,请刷新当前页面重新提交!');
        return false;
    }
    var content = '';
    if (content_type == 1) {
        content = $('#contentText').val();
    } else {
        content = $('input[name=contentLink]').val();
        var reg = /^((https|http){0,1}(:\/\/){0,1}).+$/;
        var objExp = new RegExp(reg);
        if (objExp.test(content) != true) {
            toastr.error("链接格式不正确!");
            return false;
        }
    }
    if (content == '' || content == '<p><br></p>') {
        toastr.error("内容必须填写!");
        return false;
    }

    var option = {
        title: title,
        tags: join_tags,
        image: cover_img,
        author: author,
        status: status,
        from: from,
        subject: subject,
        content_type: content_type,
        content: content,
    };

    return option;
}