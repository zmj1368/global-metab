$(function () {
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()}
    });
    // wangEditor
    var E = window.wangEditor;
    var editor = new E('#editor');
    editor.customConfig.zIndex = 0;
    editor.customConfig.uploadImgHeaders = {
        'X-CSRF-TOKEN': $('input[name="_token"]').val()
    };
    editor.customConfig.uploadImgServer = $('input[name=noticeEditor]').val();
    editor.customConfig.uploadImgMaxSize = 3 * 1024 * 1024;
    // 限制一次最多上传 5 张图片
    editor.customConfig.uploadImgMaxLength = 1;
    editor.customConfig.uploadFileName = 'file';
    // 自定义菜单配置
    editor.customConfig.menus = [
        'head',  // 标题
        'bold',  // 粗体
        'fontSize',  // 字号
        'fontName',  // 字体
        'italic',  // 斜体
        'underline',  // 下划线
        'strikeThrough',  // 删除线
        'foreColor',  // 文字颜色
        'backColor',  // 背景颜色
        'undo',  // 撤销
        'redo'  // 重复
    ];
    editor.create();

    var defaultContent = $('#content').val();
    if ('' != defaultContent) {
        editor.txt.html(defaultContent);
    }
    $('textarea[name="contentText"]').html(defaultContent);

    /**
     * 上传背景图片按钮
     */
    $('#upload').click(function () {
        $('#image_file').click();
    });

    $('#doc_photo').click(function () {
        clickImg($(this));
    });

    // 上传背景图
    var uploadImgUrl = $('input[name=uploadPhotoUrl]').val();
    $("#image_file").change(function () {
        var formData = new FormData();
        formData.append('file', document.getElementById("image_file").files[0]);
        $.ajax({
            url: uploadImgUrl,
            type: "POST",
            data: formData,
            dataType: 'json',
            cache: false,         //不设置缓存
            processData: false,  // 不处理数据
            contentType: false,   // 不设置内容类型
            success: function (res) {
                if (res.code) {
                    toastr.error(res.msg);
                } else {
                    var logo_path = uploadImgUrl + '?' + res.data.originPath;
                    $("#origin_image_url").val("");
                    $("#new_image").val(logo_path);
                    $('#doc_photo').attr('src', logo_path);
                }
            }
        });
    });

    // 提交表单
    var createFlag = 0;
    // 点击创建 提交
    $('#btn_Editor').click(function () {

        var option = checkPostData(editor);

        console.log(option);

        if (option == false) {
            return false;
        }
        var noticeStore = $('input[name=noticeStore]').val();
        var noticeSucRedirect = $('input[name=noticeSucRedirect]').val();
        if (createFlag == 0) {
            // 锁定
            createFlag++;

            // 发送请求
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()}
            });
            $.ajax({
                url: noticeStore,
                type: "PUT",
                data: option,
                success: function (result) {
                    createFlag = 0;
                    if (result.code > 0) {
                        // 请求失败
                        toastr.error(result.msg);
                        window.location.reload();
                    } else if (result.code == 0) {
                        // 请求验证通过
                        toastr.success(result.msg);
                        window.location.href = noticeSucRedirect
                    } else {
                        toastr.error('服务器错误,请重新填写！');
                    }
                },
                error: function (res) {
                    // console.log('11111111');
                    createFlag = 0;
                    var errors = res.responseJSON.errors;
                    $.each(errors, function (index, value) {
                        $.each(value, function (key, val) {
                            toastr.error(val);
                            console.log(val)
                        })
                    })
                }
            });
        }
    });

    // 点击撤销时回退首页
    $("button[type='reset']").click(function () {
        window.location.href = $('input[name=noticeSucRedirect]').val();
    });
});

function clickImg(obj) {
    $('#showImg img').attr('src', $(obj).attr('src'))
        .attr('width', $(obj).data('width'))
        .attr('height', $(obj).data('height'));
    $('#showImg').modal('show');
}

// 验证提交数据
function checkPostData(editor) {
    var name = $.trim($('input[name=name]').val());

    //专家姓名
    if (name == '') {
        toastr.error('专家姓名必填!');
        return false;
    }
    if (name.length > 10) {
        toastr.error('专家姓名不能大于10个字符!');
        return false;
    }

    // 图片
    var img = $('input[name=image]').val();

    // 专家介绍
    var content = editor.txt.text();
    if (content == "") {
        toastr.error("介绍必须填写!");
        return false;
    }
    console.log(content.length);
    if (content.length > 100) {
        toastr.error("介绍字数不能大于100字!");
        return false;
    }

    var option = {
        name: name,
        image: img,
        content: content,
    };

    return option;
}