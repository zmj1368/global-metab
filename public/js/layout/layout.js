// 公用部分js
$(function () {
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()}
    });
    // 标签
    $("#tags_list").select2({
        placeholder: "请选择标签",
        allowClear: false,
        maximumSelectionLength: 2,
    });

    // 判断正文类型
    var content_type_editor = parseInt($('input[name=content_type_editor]').val());

    // wangEditor
    var E = window.wangEditor;
    var editor = new E('#editor');
    editor.customConfig.zIndex = 0;
    editor.customConfig.uploadImgHeaders = {
        'X-CSRF-TOKEN': $('input[name="_token"]').val()
    };
    editor.customConfig.uploadImgServer = $('input[name=noticeEditor]').val();
    editor.customConfig.uploadVideoServer = $('input[name=noticeVideoEditor]').val();
    editor.customConfig.uploadImgMaxSize = 3 * 1024 * 1024;
    // 限制一次最多上传 5 张图片
    editor.customConfig.uploadImgMaxLength = 1;
    editor.customConfig.uploadFileName = 'file';

    var $text1 = $('#contentText')
    editor.customConfig.onchange = function (html) {
        // 监控变化，同步更新到 textarea
        $text1.val(html)
    };
    editor.create();
    E.fullscreen.init('#editor');

    // 判断是否为编辑状态
    var isEditor = parseInt($('input[name=isEditor]').val()) ? parseInt($('input[name=isEditor]').val()) : 0;
    if (isEditor) {
        var defaultContent = $('#content').val();
        if (content_type_editor == 1) {
            $('#contentEditor').show();
            $('#editor').show();
            $('#contentLink').hide();
            if ('' != defaultContent) {
                editor.txt.html(defaultContent);
            }
            $('textarea[name="contentText"]').html(defaultContent);
        } else {
            // 选中 content_type_id = 2
            $("input:radio[name='content_type_id'][value='2']").click();
            $('#contentEditor').hide();
            $('#editor').hide();
            $('#contentLink').show();
            $('input[name=contentLink]').val(defaultContent);
        }
    }

    // 初始化 textarea 的值
    $text1.val(editor.txt.html());

    // 点击撤销时回退首页
    $("button[type='reset']").click(function () {
        window.location.href = $('input[name=noticeSucRedirect]').val();
    });

    /**
     * 上传图片按钮
     */
    $('#upload').click(function () {
        $('#image_file').click();
    });

    $('#doc_photo').click(function () {
        clickImg($(this));
    });

    // 上传图片
    var uploadImgUrl = $('input[name=uploadPhotoUrl]').val();
    $("#image_file").change(function () {
        var formData = new FormData();
        formData.append('file', document.getElementById("image_file").files[0]);
        $.ajax({
            url: uploadImgUrl,
            type: "POST",
            data: formData,
            dataType: 'json',
            cache: false,         //不设置缓存
            processData: false,  // 不处理数据
            contentType: false,   // 不设置内容类型
            success: function (res) {
                if (res.code) {
                    toastr.error(res.msg);
                } else {
                    var logo_path = uploadImgUrl + '?' + res.data.originPath;
                    $("#origin_image_url").val("");
                    $("#new_image").val(logo_path);
                    $('#doc_photo').attr('src', logo_path);
                }
            }
        });
    });

    // 选择正文内容
    $('input[name=content_type_id]').on('change', function () {
        var type_id2 = parseInt($('input[name=content_type_id]:checked').val());
        if (type_id2 == 1) {
            $('#contentEditor').show();
            $('#editor').show();
            $('#contentLink').hide();
        } else {
            $('#contentEditor').hide();
            $('#editor').hide();
            $('#contentLink').show();
        }
    });
});

function clickImg(obj) {
    $('#showImg img').attr('src', $(obj).attr('src'))
        .attr('width', $(obj).data('width'))
        .attr('height', $(obj).data('height'));
    $('#showImg').modal('show');
}
