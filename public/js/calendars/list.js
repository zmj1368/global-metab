function hrefAdd() {
    window.location.href = "/admin/calendars/create";
}

$(function () {
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });
    $('#table').bootstrapTable({
        columns: [{
            field: 'id',
            title: 'ID',
            align: 'center',
            width: '50px',
        }, {
            field: 'title',
            title: '标题',
            align: 'center',
            width: "200px",
        }, {
            field: 'image',
            title: '背景图',
            align: 'center',
            formatter: function (value, row, index) {
                return `<img src="${value}" style="height: 65px;width: 57px">`
            }
        }, {
            field: 'created_at',
            title: '创建时间',
            align: 'center',
            width: "100px",
        }, {
            title: '操作',
            align: 'center',
            width: '200px',
            formatter: function (value, row, index) {
                return `<a class="detail" href="/admin/calendars/${row.id}/edit">编辑</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="delBtn" href="javascript:void(0);" data-id="${row.id}">删除</a>`
            }
        }],
        pageSize: 20,
        url: $('#listUrl').val(),
        method: 'post',
        dataType: 'json',
        striped: true,
        pagination: true,
        sidePagination: 'server',
        // paginationVAlign: 'top',
        paginationHAlign: 'right',
        formatRecordsPerPage: function (pageNumber) {
            return '每页显示 ' + pageNumber + ' 条数据';
        },
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return '共 ' + totalRows + ' 条数据';
        },
        queryParams: function (params) {
            // 医院
            var hosp_selector = $('#hosp_selector').val();
            // 科室
            var doc_dept = $('#doc_dept').val();
            // 姓名
            var doc_name = $('#doc_name').val();
            // 手机号
            var doc_cell = $('#doc_cell').val();
            // 参与项目
            var project_list = $('#project_list').val();
            return {
                act: $('#docType').val() ? $('#docType').val() : 'all',
                page: (params.offset / params.limit) + 1,
                size: params.limit,
                hosp_selector: hosp_selector ? hosp_selector : '',
                doc_dept: doc_dept ? doc_dept : '',
                doc_name: doc_name ? doc_name : '',
                doc_cell: doc_cell ? doc_cell : '',
                project_list: project_list ? project_list : '',
            };
        }
    });
    // enter键 搜索
    $('#title_selector').keyup(function (event) {
        if (event.keyCode == 13) {
            getList(1)
        }
        return false;
    });

    // 点击删除
    $('body').on('click', '.delBtn', function () {
        var id = $.trim($(this).attr('data-id'));
        if (parseInt(id) == 0) {
            toastr.error("该数据有误,请勿操作并联系管理人员")
            return false;
        }

        if (!id) {
            toastr.error('请指定要操作的数据！');
            return false;
        }

        $('.offlineDoc').attr('data-id', id).attr('data-status', 1);
        // 弹出模态框
        $('#modal_offlineDoctor').modal('show');
    });

    // 确认删除
    $('.offlineDoc').click(function () {
        var _this = $(this);
        if (_this.hasClass('btn-disabled')) {
            return false;
        }
        addDisabled(_this);
        var id = $(this).attr('data-id');

        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        $.ajax({
            url: $('#delUrl').val(),
            type: 'POST',
            data: {id: id},
            success: function (result) {
                removeDisabled(_this);
                // 取消模态框
                $('#modal_offlineDoctor').modal('hide');
                if (result.code > 0) {
                    toastr.error(result.msg);
                } else if (result.code == 0) {
                    // 请求通过
                    toastr.success(result.msg);
                    getList(1)
                } else {
                    toastr.error('服务器错误！');
                }
            }
        });
    });

    // 添加btn-disabled
    var addDisabled = function (obj) {
        if (!obj.hasClass('btn-disabled')) {
            obj.addClass('btn-disabled');
        }
    };

    // 移出btn-disabled
    var removeDisabled = function (obj) {
        if (obj.hasClass('btn-disabled')) {
            obj.removeClass('btn-disabled');
        }
    };

    // 点击reset
    $('.resetList').click(function () {
        window.location.reload();
    });

    $('#search_submit').click(function () {
        getList(1);
    });
});

function getList(type) {
    var params = $('#table').bootstrapTable('getOptions'), data = {
        page: params.pageNumber,
        size: params.pageSize
    };

    // 名称模糊搜索
    var title = $('#title_selector').val();

    if (type) {
        data = {
            title: title ? title : '',
            page: params.pageNumber,
            size: params.pageSize,
        };
    } else {
        data = {
            page: params.pageNumber,
            size: params.pageSize,
        };
    }

    $.ajax({
        url: $('#listUrl').val(),
        type: "POST",
        data: data,
        success: function (res) {
            $('#table').bootstrapTable('load', res);
        }
    });
}