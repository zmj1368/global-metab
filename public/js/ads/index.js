$(function () {
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()}
    });
    // 点击大图
    $('#doc_photo').click(function () {
        clickImg($(this));
    });

    // 返回button
    $('#backBtn').on('click', function () {
        window.location.href = $('input[name=adsRedirect]').val();
    });

    // 点击更新 提交
    var editFlag = 0;
    $('#btn_edit_doc').click(function () {
        var option = checkPostData();
        if (option == false) {
            return false;
        }
        var noticeStore = $('input[name=adsUpdate]').val();
        var noticeSucRedirect = $('input[name=adsRedirect]').val();
        if (editFlag == 0) {
            editFlag++;
            // 发送请求
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()}
            });
            $.ajax({
                url: noticeStore,
                type: "PUT",
                data: option,
                success: function (result) {
                    editFlag = 0;
                    if (result.code > 0) {
                        // 请求失败
                        toastr.error(result.msg);
                        window.location.reload();
                    } else if (result.code == 0) {
                        // 请求验证通过
                        toastr.success(result.msg);
                        window.location.href = noticeSucRedirect
                    } else {
                        toastr.error('服务器错误,请重新填写！');
                    }
                },
                error: function (res) {
                    var errors = res.responseJSON.errors;
                    $.each(errors, function (index, value) {
                        $.each(value, function (key, val) {
                            toastr.error(val);
                            console.log(val)
                        })
                    })
                }
            });
        }
    });

    // 新增按钮
    var createFlag = 0;
    // 点击创建 提交
    $('#btn_create_doc').click(function () {
        var option = checkPostData();
        if (option == false) {
            return false;
        }
        var noticeStore = $('input[name=adsStore]').val();
        var noticeSucRedirect = $('input[name=adsRedirect]').val();
        if (createFlag == 0) {
            createFlag++;
            // 发送请求
            // $.ajaxSetup({
            //     headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()}
            // });
            $.ajax({
                url: noticeStore,
                type: "POST",
                data: option,
                success: function (result) {
                    createFlag = 0;
                    if (result.code > 0) {
                        // 请求失败
                        toastr.error(result.msg);
                        window.location.reload();
                    } else if (result.code == 0) {
                        // 请求验证通过
                        toastr.success(result.msg);
                        window.location.href = noticeSucRedirect
                    } else {
                        toastr.error('服务器错误,请重新填写！');
                    }
                },
                error: function (res) {
                    var errors = res.responseJSON.errors;
                    $.each(errors, function (index, value) {
                        $.each(value, function (key, val) {
                            toastr.error(val);
                            console.log(val)
                        })
                    })
                }
            });
        }
    });

    /**
     * 上传图片按钮
     */
    $('#upload').click(function () {
        $('#doc_photo_file').click();
    });

    //上传图片
    var uploadImgUrl = $('input[name=uploadPhotoUrl]').val();
    $("#doc_photo_file").change(function () {
        var formData = new FormData();
        formData.append('file', document.getElementById("doc_photo_file").files[0]);
        $.ajax({
            url: uploadImgUrl,
            type: "POST",
            data: formData,
            dataType: 'json',
            cache: false,         //不设置缓存
            processData: false,  // 不处理数据
            contentType: false,   // 不设置内容类型
            success: function (res) {
                if (res.code) {
                    toastr.error(res.msg);
                } else {
                    var logo_path = uploadImgUrl + '?' + res.data.originPath;
                    $("#origin_doc_photo_url").val("");
                    $("#new_doc_photo").val(logo_path);
                    $('#doc_photo').attr('src', logo_path);
                }
            }
        });
    });
});

function clickImg(obj) {
    console.log(obj, $(obj).data('width'), $(obj).data('height'));
    $('#showImg img').attr('src', $(obj).attr('src'))
        .attr('width', $(obj).data('width'))
        .attr('height', $(obj).data('height'));
    $('#showImg').modal('show');
}

// 验证提交数据
function checkPostData() {
    // 标题
    var title = $.trim($('input[name=title]').val());
    if (title == '') {
        toastr.error('标题必填!');
        return false;
    }

    // 链接图片
    var image = $('input[name=cover_img]').val();
    if (image == "") {
        toastr.error('图片必须上传!');
        return false;
    }

    // 位置
    var spaceArr = new Array();
    $('.space').each(function () {
        if ($(this).is(":checked")) {
            spaceArr.push($(this).val());
        }
    });
    if (spaceArr.length == 0) {
        toastr.error('位置必须选择!');
        return false;
    }

    // 优先级
    var level = parseInt($.trim($('input[name=level]').val()));
    if (level < 0) {
        toastr.error('排序不能小于0!');
        return false;
    }
    // 状态
    var status = parseInt($('#status').find("option:selected").val());
    if (status != 1 && status != 0) {
        toastr.error('提交出错,请刷新当前页面重新提交!');
        return false;
    }

    // 链接
    var link = $.trim($('input[name=link]').val());
    var option = {
        title: title,
        space: spaceArr,
        link: link,
        sort: level,
        image: image,
        status: status,
    };

    return option;
}