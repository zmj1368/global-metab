$(function () {
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()}
    });

    $('#reset,#search').click(function () {
        if (!$(this).data('type')) {
            $('#searchSpace').val(0);
            $('#searchStatus').val(-1);
            $('.searchTitle').val('')
        }
        getList($(this).data('type'));
    });

    $('#table').bootstrapTable({
        columns: [{
            field: 'id',
            title: 'ID',
            align: 'center',
            width: '50px',
        }, {
            field: 'title',
            title: '标题',
            align: 'center',
            width: "200px",
        }, {
            field: 'image',
            title: '图片',
            align: 'center',
            formatter: function (value, row, index) {
                return `<img src="${value}" style="height: 65px;width: 57px">`;
            }
        }, {
            field: 'spaceName',
            title: '位置',
            align: 'center',
            width: '150px'
        }, {
            field: 'sort',
            title: '排序',
            align: 'center',
            width: '50px'
        }, {
            field: 'status',
            title: '状态',
            align: 'center',
            width: '50px',
            formatter: function (value, row, index) {
                return value == 1 ? '显示' : '隐藏';
            }
        }, {
            field: 'created_at',
            title: '创建时间',
            align: 'center',
            width: '100px',
        }, {
            title: '操作',
            align: 'center',
            formatter: function (value, row, index) {
                return '<a title="' + row.name + '" href="/admin/ads/' + row.id + '/edit">编辑</a>&nbsp;&nbsp;&nbsp;&nbsp;' + '<a class="delBtn" href="javascript:void(0);" data-id="'+ row.id +'">删除</a> ';
            }
        }],
        pageSize: 20,
        url: $('#listUrl').val(),
        method: 'post',
        dataType: 'json',
        striped: true,
        pagination: true,
        sidePagination: 'server',
        paginationHAlign: 'right',
        formatRecordsPerPage: function (pageNumber) {
            return '每页显示 ' + pageNumber + ' 条数据';
        },
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return '共 ' + totalRows + ' 条数据';
        },
        queryParams: function (params) {
            return {
                searchTitle: $.trim($('#searchTitle').val()) ? $.trim($('#searchTitle').val()) : '',
                searchSpace: $('#searchSpace').val(),
                searchStatus: $('#searchStatus').val(),
                page: (params.offset / params.limit) + 1,
                size: params.limit
            };
        }
    });

    // 点击删除
    $('body').on('click', '.delBtn', function () {
        var id = $.trim($(this).attr('data-id'));
        if (parseInt(id) == 0) {
            toastr.error("该数据有误,请勿操作并联系管理人员")
            return false;
        }

        if (!id) {
            toastr.error('请指定要操作的数据！');
            return false;
        }

        $('.offlineDoc').attr('data-id', id).attr('data-status', 1);
        // 弹出模态框
        $('#modal_offlineDoctor').modal('show');
    });

    // 确认删除
    $('.offlineDoc').click(function () {
        var _this = $(this);
        if (_this.hasClass('btn-disabled')) {
            return false;
        }
        addDisabled(_this);
        var id = $(this).attr('data-id');

        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        $.ajax({
            url: $('#delUrl').val(),
            type: 'POST',
            data: {id: id},
            success: function (result) {
                removeDisabled(_this);
                // 取消模态框
                $('#modal_offlineDoctor').modal('hide');
                if (result.code > 0) {
                    toastr.error(result.msg);
                } else if (result.code == 0) {
                    // 请求通过
                    toastr.success(result.msg);
                    getList(1)
                } else {
                    toastr.error('服务器错误！');
                }
            }
        });
    });

    // 添加btn-disabled
    var addDisabled = function (obj) {
        if (!obj.hasClass('btn-disabled')) {
            obj.addClass('btn-disabled');
        }
    };

    // 移出btn-disabled
    var removeDisabled = function (obj) {
        if (obj.hasClass('btn-disabled')) {
            obj.removeClass('btn-disabled');
        }
    };
});


function getList(type) {
    var params = $('#table').bootstrapTable('getOptions'), data = {
        page: params.pageNumber,
        size: params.pageSize
    };
    if (type) {
        data = {
            searchTitle: $.trim($('#searchTitle').val()) ? $.trim($('#searchTitle').val()) : '',
            searchSpace: $('#searchSpace').val(),
            searchStatus: $('#searchStatus').val(),
            page: params.pageNumber,
            size: params.pageSize
        };
    } else {
        data = {
            searchTitle: '',
            searchSpace: 0,
            searchStatus: -1,
            page: params.pageNumber,
            size: params.pageSize
        }
    }

    $.ajax({
        url: $('#listUrl').val(),
        type: "POST",
        data: data,
        success: function (res) {
            $('#table').bootstrapTable('load', res);
        }
    });
}
