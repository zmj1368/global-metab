<?php

return [
    'root' => empty(env('UPLOAD_IMAGE_ROOT')) ? public_path('') : env('UPLOAD_IMAGE_ROOT'),
    
    /**
     * 运营通知、市场通知、资料库、文章管理 图片上传配置
     */
    'image' => [
        'path' => '/uploads',
        'width' => 327,
        'height' => 139,
        'size' => 2 * 1024 * 1024,
        'extra' => ['jpg', 'png', 'gif', 'jpeg'],
    ],
];