<?php

return [
    'root' => public_path(''),

    /**
     * 上传video 配置
     */
    'video' => [
        'path' => '/uploads/videos',
        'size' => 500 * 1024 * 1024,
        'extra' => ["avi", "mp4", "3gp", "mpeg", "mpg", "dat", "mov", "rmvb", "rm", "flv"],
    ],

    /**
     * 上传oss配置
     */
    'oss' => [
        'path' => 'user/system/videos'
    ]
];